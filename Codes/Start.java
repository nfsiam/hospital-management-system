import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class Start extends JFrame implements ActionListener
{

	
	JButton exitbutton, loginbutton;
	JLabel hospital;
	JPanel panel;
	
	Font font = new Font("Times New Roman", Font.PLAIN, 50);

	Color kasper = new Color(0,167,157);
	Color blue = new Color (0,120,215);
	Color bloodLake = new Color(220,90,73);

	public Start(){
		
		super("Hospital Management System");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(380,200,1120,630);
		

		panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(Color.white);




		hospital = new JLabel("Hospital Management System");
		hospital.setBounds(260,100,1000,100);
		hospital.setFont(font);
		hospital.setForeground(kasper);
		panel.add(hospital);


		exitbutton = new JButton("Exit");
		exitbutton.setBounds(335,300,200,50);
		exitbutton.addActionListener(this);
		exitbutton.setBackground(Color.white);
		panel.add(exitbutton);

		loginbutton = new JButton("Login");
		loginbutton.setBounds(585,300,200,50);
		loginbutton.setBackground(Color.white);
		loginbutton.addActionListener(this);
		panel.add(loginbutton);


		this.add(panel);


	}

	public void actionPerformed(ActionEvent a){

		if((JButton)a.getSource()==exitbutton){
			
			System.exit(0);
		}
		else if((JButton)a.getSource()==loginbutton){

			LoginFrame obj = new LoginFrame();
			this.setVisible(false);
			obj.setVisible(true);

			
		}

	}

	public static void main(String []args)
	{
		Start object = new Start();
		object.setVisible(true);
	}
}