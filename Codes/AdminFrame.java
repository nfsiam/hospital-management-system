import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.*;
import java.awt.*;
import java.sql.*;
import java.util.*;


public class AdminFrame extends JFrame implements ActionListener, FocusListener

{
	LoginFrame lf;
	String currentUserName;
	ImageIcon backGroundIcon = new ImageIcon("res/docback6.png");
	JLabel backGroundLabel = new JLabel(backGroundIcon) ;

	Font f1 = new Font("Calibri",Font.PLAIN,15);
	Font f2 = new Font("Calibri",Font.PLAIN,20);
	Font f3 = new Font("Calibri",Font.PLAIN,25);
	Font f4 = new Font("Calibri",Font.BOLD,35);




	Color kasper = new Color(0,167,157);
	Color kasper2 = new Color(45,211,201);
	Color bloodLake = new Color(220,90,73);


	JPanel  sideBar, clickedBar;

	JPanel removePanel,addPanel, updatePanel,viewPanel, myPanel;
	JButton backButton = new JButton("BACK"),tempButton;
	JLayeredPane adminPanel;


	JButton viewProfiles, manageProfiles ,logoutButton, addProfiles, updateProfiles, removeProfiles, myProfileBtn;

	JButton []profileButtonsArray = new JButton[5];

	JLabel adminHeader,currentUserLabel;


	JLabel userNameLabelRP;
	JButton removeUserRP,findUserRP;
	JTextField userNameTFRP;

	
	JTextField userTFAP, phoneTFAP, eNameTFAP, salaryTFAP;
	JLabel userLabelAP, eNameLabelAP, phoneLabelAP, roleLabelAP, salaryLabelAP;

	JTable myTable;
	JScrollPane tableScrollPane;

	int flagRP=0;

	JLabel passLabelAP;
	JTextField passTFAP, phoneTF1;
	JComboBox roleComboAP;
	JButton autoPassBtnAP, addBtnAP, clearBtnAP;




	JLabel userLabelUP, eNameLabelUP, phoneLabelUP, roleLabelUP, salaryLabelUP;
	JTextField userTFUP, eNameTFUP, phoneTFUP, roleTFUP, salaryTFUP;
	JButton loadUserUP,updateBtnUP, clearBtnUP;
	String tempID;
	int flagUP=0;


	JTable tTable;
	JScrollPane tScroll;



	JLabel userLabelMP, eNameLabelMP,phoneLabelMP,passLabelMP;
	JTextField userTFMP, eNameTFMP, phoneTFMP;
	JButton clearBtnMP, updateBtnMP;
	JPasswordField passPFMP;





	public AdminFrame(LoginFrame lf, String currentUserName)
	{
		super("Hospital Management System");
		this.lf=lf;
		this.currentUserName=currentUserName;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1120,630);
		setLocationRelativeTo(null);

		backGroundLabel.setBounds(0,0,1120,630);

		

		initAdminPanel();
		//initViewPanel();
		//initAddPanel();
		//initRemovePanel();
		//initUpdatePanel();
		//initMyPanel();
		//clickedBar.add(myPanel);
		defaultMethod();

	}
	public void initAdminPanel()
	{
		adminPanel = new JLayeredPane();
		adminPanel.setLayout(null);

		sideBar = new JPanel();
		sideBar.setLayout(null);
		sideBar.setBounds(0,69,150,580);
		sideBar.setOpaque(false);
		

		clickedBar = new JPanel();
		clickedBar.setLayout(new GridLayout(1,1));
		clickedBar.setBounds(150,70,970,580);
		clickedBar.setOpaque(false);
		//clickedBar.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));


		backButton.setBounds(500,200,100,100);
		backButton.addActionListener(this);

		adminHeader = new JLabel(" Manager's Desk");
		adminHeader.setBounds(0,0,1120,70);
		adminHeader.setFont(f4);
		adminHeader.setOpaque(true);
		adminHeader.setForeground(kasper);
		adminHeader.setBackground(Color.white);
		adminHeader.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, kasper));
		adminHeader.setVerticalAlignment(JLabel.CENTER);
		adminHeader.setHorizontalAlignment(JLabel.LEFT);


		currentUserLabel = new JLabel();
		currentUserLabel.setBounds(300,15,682,40);
		currentUserLabel.setFont(f2);
		currentUserLabel.setForeground(Color.gray);
		currentUserLabel.setBackground(Color.gray);
		currentUserLabel.setVerticalAlignment(JLabel.CENTER);
		currentUserLabel.setHorizontalAlignment(JLabel.RIGHT);


		logoutButton = new JButton("Logout");
		logoutButton.setBounds(997,15,100,40);
		logoutButton.setFont(f2);
		logoutButton.setForeground(bloodLake);
		logoutButton.setBackground(Color.white);
		logoutButton.setFocusPainted(false);
		logoutButton.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.gray));
		logoutButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		logoutButton.addActionListener(this);


		myProfileBtn = new JButton("My Profile");
		myProfileBtn.setBounds(0,40,150,40);
		myProfileBtn.setFont(f1);
		myProfileBtn.setBackground(Color.white);
		myProfileBtn.setForeground(Color.gray);
		myProfileBtn.setFocusPainted(false);
		myProfileBtn.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		myProfileBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		myProfileBtn.addActionListener(this);

		viewProfiles = new JButton("View Profiles");
		viewProfiles.setBounds(0,40+40,150,40);
		viewProfiles.setFont(f1);
		viewProfiles.setBackground(Color.white);
		viewProfiles.setForeground(Color.gray);
		viewProfiles.setFocusPainted(false);
		viewProfiles.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		viewProfiles.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		viewProfiles.addActionListener(this);

		addProfiles = new JButton("Add Profiles");
		addProfiles.setBounds(0,80+40,150,40);
		addProfiles.setFont(f1);
		addProfiles.setBackground(Color.white);
		addProfiles.setForeground(Color.gray);
		addProfiles.setFocusPainted(false);
		addProfiles.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		addProfiles.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		addProfiles.addActionListener(this);

		updateProfiles = new JButton("Update Profiles");
		updateProfiles.setBounds(0,120+40,150,40);
		updateProfiles.setFont(f1);
		updateProfiles.setBackground(Color.white);
		updateProfiles.setForeground(Color.gray);
		updateProfiles.setFocusPainted(false);
		updateProfiles.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		updateProfiles.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		updateProfiles.addActionListener(this);

		removeProfiles = new JButton("Remove Profiles");
		removeProfiles.setBounds(0,160+40,150,40);
		removeProfiles.setFont(f1);
		removeProfiles.setBackground(Color.white);
		removeProfiles.setForeground(Color.gray);
		removeProfiles.setFocusPainted(false);
		removeProfiles.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		removeProfiles.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		removeProfiles.addActionListener(this);


		profileButtonsArray[0]=myProfileBtn;
		profileButtonsArray[1]=viewProfiles;
		profileButtonsArray[2]=addProfiles;
		profileButtonsArray[3]=updateProfiles;
		profileButtonsArray[4]=removeProfiles;

		

		sideBar.add(myProfileBtn);
		sideBar.add(viewProfiles);
		sideBar.add(addProfiles);
		sideBar.add(updateProfiles);
		sideBar.add(removeProfiles);

		adminPanel.add(sideBar);
		adminPanel.add(backGroundLabel, Integer.valueOf(1));

    	adminPanel.add(logoutButton, Integer.valueOf(4));
    	adminPanel.add(currentUserLabel,Integer.valueOf(3));
    	adminPanel.add(adminHeader, Integer.valueOf(2));
    	adminPanel.add(sideBar, Integer.valueOf(5));
    	adminPanel.add(clickedBar, Integer.valueOf(6));


		add(adminPanel);
	}

	public void initMyPanel()
	{
		myPanel = new JPanel();
		myPanel.setLayout(null);
		myPanel.setOpaque(false);

		userLabelMP = new JLabel("   User Name");
		userLabelMP.setBounds(150,40,120,40);
		userLabelMP.setFont(f1);
		userLabelMP.setOpaque(true);
		userLabelMP.setBackground(Color.white);
		userLabelMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		userLabelMP.setHorizontalAlignment(SwingConstants.LEFT);
		userLabelMP.setForeground(kasper);
		myPanel.add(userLabelMP);
		
		userTFMP = new JTextField(currentUserName);
		userTFMP.setBounds(270,40,490,40);
		userTFMP.setFont(f2);
		userTFMP.setBackground(Color.white);
		userTFMP.setForeground(Color.gray);
		userTFMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		userTFMP.setHorizontalAlignment(SwingConstants.LEFT);
		userTFMP.setEditable(false);
		myPanel.add(userTFMP);

		
		eNameLabelMP = new JLabel("   Employee Name");
		eNameLabelMP.setBounds(150,100,120,40);
		eNameLabelMP.setFont(f1);
		eNameLabelMP.setOpaque(true);
		eNameLabelMP.setBackground(Color.white);
		eNameLabelMP.setForeground(kasper);
		eNameLabelMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		myPanel.add(eNameLabelMP);
		
		eNameTFMP = new JTextField();
		eNameTFMP.setBounds(270,100,490,40);
		eNameTFMP.setFont(f2);
		eNameTFMP.setBackground(Color.white);
		eNameTFMP.setForeground(Color.GRAY);
		eNameTFMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		eNameTFMP.setHorizontalAlignment(SwingConstants.LEFT);
		myPanel.add(eNameTFMP);
		
		phoneLabelMP = new JLabel("   Phone");
		phoneLabelMP.setBounds(150,160,120,40);
		phoneLabelMP.setFont(f1);
		phoneLabelMP.setOpaque(true);
		phoneLabelMP.setBackground(Color.white);
		phoneLabelMP.setForeground(kasper);
		phoneLabelMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		myPanel.add(phoneLabelMP);
		
		phoneTFMP = new JTextField();
		phoneTFMP.setBounds(270,160,490,40);
		phoneTFMP.setFont(f2);
		phoneTFMP.setBackground(Color.white);
		phoneTFMP.setForeground(Color.GRAY);
		phoneTFMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		phoneTFMP.setHorizontalAlignment(SwingConstants.LEFT);
		myPanel.add(phoneTFMP);



		passLabelMP = new JLabel("   Password");
		passLabelMP.setBounds(150,220,120,40);
		passLabelMP.setFont(f1);
		passLabelMP.setOpaque(true);
		passLabelMP.setBackground(Color.white);
		passLabelMP.setForeground(kasper);
		passLabelMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		myPanel.add(passLabelMP);
		
		passPFMP = new JPasswordField();
		passPFMP.setBounds(270,220,490,40);
		passPFMP.setFont(f2);
		passPFMP.setBackground(Color.white);
		passPFMP.setForeground(Color.GRAY);
		passPFMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		passPFMP.setHorizontalAlignment(SwingConstants.LEFT);
		passPFMP.addFocusListener(this);
		myPanel.add(passPFMP);

		
		clearBtnMP = new JButton("Clear");
		clearBtnMP.setBounds(150, 300, 305, 40);
		clearBtnMP.setFont(f1);
		clearBtnMP.setForeground(Color.gray);
		clearBtnMP.setBackground(Color.white);
		clearBtnMP.setFocusPainted(false);
		clearBtnMP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		clearBtnMP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		clearBtnMP.addActionListener(this);
		myPanel.add(clearBtnMP);
		


		updateBtnMP = new JButton("Update");
		updateBtnMP.setBounds(455, 300, 305, 40);
		updateBtnMP.setFont(f1);
		updateBtnMP.setForeground(kasper);
		updateBtnMP.setBackground(Color.white);
		updateBtnMP.setFocusPainted(false);
		updateBtnMP.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, kasper));
		updateBtnMP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		updateBtnMP.addActionListener(this);
		myPanel.add(updateBtnMP);


		myInfo();
		logPortion();
	}

	public void myInfo()
	{

		String loadId = userTFMP.getText();
		///tempID=loadId;
		String query = "SELECT `employeeName`, `phoneNumber` FROM `employee` WHERE `userName`='"+loadId+"';";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;
			String eName = null;
			String phnNo = null;
					
			while(rs.next())
			{
                eName = rs.getString("employeeName");
				phnNo = rs.getString("phoneNumber");
				flag=true;
				
				eNameTFMP.setText(eName);
				currentUserLabel.setText(eName);
				currentUserLabel.repaint();
				phoneTFMP.setText(phnNo);
				
				updateBtnMP.setEnabled(true);
				flagUP=1;

				logPortion();
				//delBtn.setEnabled(true);
			}
			if(!flag)
			{
				eNameTFMP.setText("");
				phoneTFMP.setText("");
				updateBtnMP.setEnabled(false);
				

				JOptionPane.showMessageDialog(this,"Invalid ID"); 
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }


	}


	public void logPortion()
	{


		String loadId =userTFMP.getText();
		///tempID=loadId;
		String query = "SELECT `Password` FROM `login` WHERE `userName`='"+loadId+"';";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;
			String pass = null;
					
			while(rs.next())
			{
                pass = rs.getString("Password");
				flag=true;
				
				passPFMP.setText(pass);
				
				updateBtnMP.setEnabled(true);
				flagUP=1;
				//delBtn.setEnabled(true);
			}
			if(!flag)
			{
				eNameTFMP.setText("");
				phoneTFMP.setText("");
				updateBtnMP.setEnabled(false);
				

				JOptionPane.showMessageDialog(this,"Invalid ID"); 
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }

	}

	public void updateMyDB()
	{
		
		String newId;
		String eName;
		String phnNo;
		String pass;

		try
		{
			newId = userTFMP.getText();
			eName = eNameTFMP.getText();
			phnNo = phoneTFMP.getText();
			pass =  passPFMP.getText();

		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(this, "Plase check inserted iformations again");
			return;
		}
		String query = "UPDATE employee SET employeeName='"+eName+"', phoneNumber = '"+phnNo+"' WHERE userName='"+newId+"'";
		//System.out.println(status);
		String query2 = "UPDATE Login SET Password="+pass+" WHERE userName='"+newId+"'";
		
        Connection con=null;//for connection
        Statement st = null;//for query execution
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			st = con.createStatement();//create statement
			st.executeUpdate(query);
			st.executeUpdate(query2);
			st.close();
			con.close();


			currentUserLabel.setText(eName);

			currentUserLabel.repaint();
			JOptionPane.showMessageDialog(this, "Account Updated Successfully");
			
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(this, "Sorry! Something Went Wrong");
		}
	}

	public void initViewPanel()
	{

		viewPanel = new JPanel();
		viewPanel.setLayout(null);
		viewPanel.setOpaque(false);



		String query = "SELECT *  FROM `employee`;"; 

		//"SELECT `employeeName`, `phoneNumber`, `role`, `salary` FROM `employee`;"; 

        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try 
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");


			
			//String data[][] = {};
			int c=0;
			String[][] data = new String[100][5];
			String column[] = {"User Name", "Name", "Phone no.","Role","Salary"};

			int j,z=0;
			for(int i = 0; rs.next(); i++){
				
				
				j = 0;
				data[i][j++] = rs.getString("UserName");
				data[i][j++] = rs.getString("employeeName");
				data[i][j++] = rs.getString("phoneNumber");
				data[i][j++] = rs.getString("role");
				data[i][j++] = rs.getString("salary");

				c++;
				z=z+40;
			}

			String [][] newData = new String[c][5];
			for(int i=0; i<c; i++)
			{
				for(int j2=0; j2<5; j2++)
				{
					newData[i][j2]=data[i][j2];
				}
			    
			}


			  

			tTable = new JTable(newData, column){
			    DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

			    { // initializer block
			        renderRight.setHorizontalAlignment(SwingConstants.CENTER);
			    }

			    @Override
			    public TableCellRenderer getCellRenderer (int arg0, int arg1) {
			        return renderRight;
			    };
			};
			
			tTable.setFont(f1);
			tTable.setOpaque(false);
			tTable.setForeground(Color.gray);
			tTable.setBackground(Color.white);
			tTable.setRowHeight(tTable.getRowHeight()+20);
			//tTable.setColumnWidth(tTable.getColumnWidth()+20);
    		tScroll = new JScrollPane(tTable);
			tScroll.setBounds(50,40,850,450);          

		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }

		
		viewPanel.add(tScroll);
	}

	public void initAddPanel()
	{
		addPanel = new JPanel();
		addPanel.setLayout(null);
		addPanel.setOpaque(false);


		userLabelAP = new JLabel("  User Name");
		userLabelAP.setBounds(150,40,120,40);
		userLabelAP.setFont(f1);
		userLabelAP.setOpaque(true);
		userLabelAP.setBackground(Color.white);
		userLabelAP.setForeground(kasper);


		addPanel.add(userLabelAP);
		
		userTFAP = new JTextField();
		userTFAP.setBounds(270,40,490,40);
		userTFAP.setFont(f2);
		userTFAP.setBackground(Color.white);
		userTFAP.setForeground(Color.GRAY);
		userTFAP.setBorder(BorderFactory.createEmptyBorder());
		userTFAP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		userTFAP.setHorizontalAlignment(SwingConstants.LEFT);
		addPanel.add(userTFAP);

		
		passLabelAP = new JLabel("   Password");
		passLabelAP.setBounds(150,100,120,40);
		passLabelAP.setFont(f1);
		passLabelAP.setOpaque(true);
		passLabelAP.setBackground(Color.white);
		passLabelAP.setForeground(kasper);
		addPanel.add(passLabelAP);
		
		passTFAP = new JTextField();
		passTFAP.setBounds(270,100,390,40);
		passTFAP.setFont(f2);
		passTFAP.setBackground(Color.white);
		passTFAP.setForeground(Color.GRAY);
		passTFAP.setBorder(BorderFactory.createEmptyBorder());
		passTFAP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		passTFAP.setHorizontalAlignment(SwingConstants.LEFT);
		passTFAP.setEnabled(false);
		addPanel.add(passTFAP);
		
		autoPassBtnAP = new JButton("Generate");
		autoPassBtnAP.setBounds(660, 100, 100, 40);
		autoPassBtnAP.setFont(f1);
		autoPassBtnAP.setBackground(Color.white);
		autoPassBtnAP.setForeground(kasper);
		autoPassBtnAP.setFocusPainted(false);
		autoPassBtnAP.setBorder(BorderFactory.createEmptyBorder());
		autoPassBtnAP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		autoPassBtnAP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		autoPassBtnAP.addActionListener(this);
		addPanel.add(autoPassBtnAP);
		
		eNameLabelAP = new JLabel("   Employee Name");
		eNameLabelAP.setBounds(150,160,120,40);
		eNameLabelAP.setFont(f1);
		eNameLabelAP.setOpaque(true);
		eNameLabelAP.setBackground(Color.white);
		eNameLabelAP.setForeground(kasper);
		addPanel.add(eNameLabelAP);
		
		eNameTFAP = new JTextField();
		eNameTFAP.setBounds(270,160,490,40);
		eNameTFAP.setFont(f2);
		eNameTFAP.setBackground(Color.white);
		eNameTFAP.setForeground(Color.GRAY);
		eNameTFAP.setBorder(BorderFactory.createEmptyBorder());
		eNameTFAP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		eNameTFAP.setHorizontalAlignment(SwingConstants.LEFT);
		addPanel.add(eNameTFAP);
		
		phoneLabelAP = new JLabel("   Phone");
		phoneLabelAP.setBounds(150,220,120,40);
		phoneLabelAP.setFont(f1);
		phoneLabelAP.setOpaque(true);
		phoneLabelAP.setBackground(Color.white);
		phoneLabelAP.setForeground(kasper);
		addPanel.add(phoneLabelAP);
		
		phoneTFAP = new JTextField();
		//phoneTF1.setEnabled(false);
		phoneTFAP.setBounds(270,220,490,40);
		phoneTFAP.setFont(f2);
		phoneTFAP.setBackground(Color.white);
		phoneTFAP.setForeground(Color.GRAY);
		phoneTFAP.setBorder(BorderFactory.createEmptyBorder());
		phoneTFAP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		phoneTFAP.setHorizontalAlignment(SwingConstants.LEFT);
		addPanel.add(phoneTFAP);
		
		
		
		roleLabelAP = new JLabel("   Role");
		roleLabelAP.setBounds(150,280,120,40);
		roleLabelAP.setFont(f1);
		roleLabelAP.setOpaque(true);
		roleLabelAP.setBackground(Color.white);
		roleLabelAP.setForeground(kasper);
		addPanel.add(roleLabelAP);
		
		String []items = {"Manager","Receptionist", "Doctor","Nurse"};
		roleComboAP = new JComboBox(items);
		roleComboAP.setBounds(270,280,490,40);
		roleComboAP.setFont(f1);
		roleComboAP.setOpaque(true);
		roleComboAP.setBackground(Color.white);
		roleComboAP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.white));
		((JLabel)roleComboAP.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
		roleComboAP.setForeground(kasper);
		addPanel.add(roleComboAP);
		
		salaryLabelAP = new JLabel("   Salary");
		salaryLabelAP.setBounds(150,340,120,40);
		salaryLabelAP.setFont(f1);
		salaryLabelAP.setOpaque(true);
		salaryLabelAP.setBackground(Color.white);
		salaryLabelAP.setForeground(kasper);
		addPanel.add(salaryLabelAP);
		
		salaryTFAP = new JTextField();
		salaryTFAP.setBounds(270,340,490,40);
		salaryTFAP.setFont(f2);
		salaryTFAP.setBackground(Color.white);
		salaryTFAP.setForeground(Color.GRAY);
		salaryTFAP.setBorder(BorderFactory.createEmptyBorder());
		salaryTFAP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		salaryTFAP.setHorizontalAlignment(SwingConstants.LEFT);
		addPanel.add(salaryTFAP);
		
		
		clearBtnAP = new JButton("Clear");
		clearBtnAP.setBounds(149, 400, 305, 40);
		clearBtnAP.setFont(f1);
		clearBtnAP.setForeground(Color.gray);
		clearBtnAP.setBackground(Color.white);
		clearBtnAP.setFocusPainted(false);
		clearBtnAP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		clearBtnAP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		clearBtnAP.addActionListener(this);
		addPanel.add(clearBtnAP);



		addBtnAP = new JButton("Add");
		addBtnAP.setBounds(455, 400, 306, 40);
		addBtnAP.setFont(f1);
		addBtnAP.setForeground(kasper);
		addBtnAP.setBackground(Color.white);
		addBtnAP.setFocusPainted(false);
		addBtnAP.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, kasper));
		addBtnAP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		addBtnAP.addActionListener(this);
		addPanel.add(addBtnAP);

	}

	public void initUpdatePanel()
	{
		updatePanel = new JPanel();
		updatePanel.setLayout(null);
		updatePanel.setOpaque(false);







		userLabelUP = new JLabel("User Name : ");
		userLabelUP = new JLabel("   User Name");
		userLabelUP.setBounds(150,40,120,40);
		userLabelUP.setFont(f1);
		userLabelUP.setOpaque(true);
		userLabelUP.setBackground(Color.white);
		userLabelUP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		userLabelUP.setHorizontalAlignment(SwingConstants.LEFT);
		userLabelUP.setForeground(kasper);


		updatePanel.add(userLabelUP);
		
		userTFUP = new JTextField();
		userTFUP.setBounds(270,40,340,40);
		userTFUP.setFont(f2);
		userTFUP.setBackground(Color.white);
		//userTFUP.setOpaque(false);
		userTFUP.setForeground(Color.gray);
		userTFUP.setBorder(BorderFactory.createEmptyBorder());
		userTFUP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		userTFUP.setHorizontalAlignment(SwingConstants.LEFT);
		updatePanel.add(userTFUP);


		loadUserUP = new JButton("Load Info");
		loadUserUP.setBounds(610,40,150,40);
		loadUserUP.setFont(f1);
		loadUserUP.setForeground(Color.WHITE);
		loadUserUP.setBackground(Color.white);
		loadUserUP.setForeground(kasper);
		loadUserUP.setFocusPainted(false);
		loadUserUP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		loadUserUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		loadUserUP.addActionListener(this);
		updatePanel.add(loadUserUP);


		
		
		
		eNameLabelUP = new JLabel("   Employee Name");
		eNameLabelUP.setBounds(150,100,120,40);
		eNameLabelUP.setFont(f1);
		eNameLabelUP.setOpaque(true);
		eNameLabelUP.setBackground(Color.white);
		eNameLabelUP.setForeground(kasper);
		eNameLabelUP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		updatePanel.add(eNameLabelUP);
		
		eNameTFUP = new JTextField();
		eNameTFUP.setBounds(270,100,490,40);
		eNameTFUP.setFont(f2);
		//eNameTFUP.setOpaque(false);
		eNameTFUP.setBackground(Color.white);
		eNameTFUP.setForeground(Color.GRAY);
		eNameTFUP.setBorder(BorderFactory.createEmptyBorder());
		eNameTFUP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		eNameTFUP.setHorizontalAlignment(SwingConstants.LEFT);
		updatePanel.add(eNameTFUP);
		
		phoneLabelUP = new JLabel("   Phone");
		phoneLabelUP.setBounds(150,160,120,40);
		phoneLabelUP.setFont(f1);
		phoneLabelUP.setOpaque(true);
		phoneLabelUP.setBackground(Color.white);
		phoneLabelUP.setForeground(kasper);
		phoneLabelUP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		updatePanel.add(phoneLabelUP);
		
		phoneTFUP = new JTextField();
		phoneTFUP.setBounds(270,160,490,40);
		phoneTFUP.setFont(f2);
		//phoneTFUP.setOpaque(false);
		phoneTFUP.setBackground(Color.white);
		phoneTFUP.setForeground(Color.GRAY);
		phoneTFUP.setBorder(BorderFactory.createEmptyBorder());
		phoneTFUP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		phoneTFUP.setHorizontalAlignment(SwingConstants.LEFT);
		updatePanel.add(phoneTFUP);
		
		
		
		roleLabelUP = new JLabel("   Role");
		roleLabelUP.setBounds(150,220,120,40);
		roleLabelUP.setFont(f1);
		roleLabelUP.setOpaque(true);
		roleLabelUP.setBackground(Color.white);
		roleLabelUP.setForeground(kasper);
		roleLabelUP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		updatePanel.add(roleLabelUP);

		roleTFUP = new JTextField();
		roleTFUP.setBounds(270,220,490,40);
		roleTFUP.setFont(f2);
		//roleTFUP.setOpaque(false);
		roleTFUP.setBackground(Color.white);
		roleTFUP.setForeground(Color.GRAY);
		roleTFUP.setBorder(BorderFactory.createEmptyBorder());
		roleTFUP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		roleTFUP.setHorizontalAlignment(SwingConstants.LEFT);
		updatePanel.add(roleTFUP);
		
		
		
		salaryLabelUP = new JLabel("   Salary");
		salaryLabelUP.setBounds(150,280,120,40);
		salaryLabelUP.setFont(f1);
		salaryLabelUP.setOpaque(true);
		salaryLabelUP.setBackground(Color.white);
		salaryLabelUP.setForeground(kasper);
		salaryLabelUP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		updatePanel.add(salaryLabelUP);
		
		salaryTFUP = new JTextField();
		salaryTFUP.setBounds(270,280,490,40);
		salaryTFUP.setFont(f2);
		//salaryTFUP.setOpaque(false);
		salaryTFUP.setBackground(Color.white);
		salaryTFUP.setForeground(Color.GRAY);
		salaryTFUP.setBorder(BorderFactory.createEmptyBorder());
		salaryTFUP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		salaryTFUP.setHorizontalAlignment(SwingConstants.LEFT);
		updatePanel.add(salaryTFUP);

		
		clearBtnUP = new JButton("Clear");
		clearBtnUP.setBounds(150, 340, 305, 40);
		clearBtnUP.setFont(f1);
		clearBtnUP.setForeground(Color.gray);
		clearBtnUP.setBackground(Color.white);
		clearBtnUP.setFocusPainted(false);
		clearBtnUP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		clearBtnUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		clearBtnUP.addActionListener(this);
		updatePanel.add(clearBtnUP);
		


		updateBtnUP = new JButton("Update");
		updateBtnUP.setBounds(455, 340, 305, 40);
		updateBtnUP.setFont(f1);
		updateBtnUP.setForeground(kasper);
		updateBtnUP.setBackground(Color.white);
		updateBtnUP.setFocusPainted(false);
		updateBtnUP.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, kasper));
		updateBtnUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		updateBtnUP.addActionListener(this);
		updatePanel.add(updateBtnUP);
		
	}

	public void initRemovePanel()
	{
		removePanel = new JPanel();
		removePanel.setLayout(null);
		removePanel.setOpaque(false);
		//removePanel.setBackground(Color.white);

		userNameLabelRP = new JLabel(" User Name");
		userNameLabelRP.setBounds(50,40,110,40);
		userNameLabelRP.setFont(f1);
		userNameLabelRP.setOpaque(true);
		userNameLabelRP.setBackground(Color.white);
		userNameLabelRP.setForeground(kasper);

		userNameTFRP = new JTextField();
		userNameTFRP.setBounds(160,40,585,40);
		userNameTFRP.setFont(f2);
		//userNameTFRP.setOpaque(true);
		userNameTFRP.setBackground(Color.white);
		userNameTFRP.setForeground(Color.GRAY);
		userNameTFRP.setBorder(BorderFactory.createEmptyBorder());
		userNameTFRP.setHorizontalAlignment(SwingConstants.CENTER);

		findUserRP = new JButton("Find");
		findUserRP.setBounds(745,40,150,40);
		findUserRP.setFont(f1);
		findUserRP.setBackground(Color.white);
		findUserRP.setForeground(kasper);
		findUserRP.setFocusPainted(false);
		findUserRP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		findUserRP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		findUserRP.addActionListener(this);

		removeUserRP = new JButton("Remove Employee");
		removeUserRP.setBounds(50,250,850,40);
		removeUserRP.setFont(f2);
		removeUserRP.setBackground(bloodLake);
		removeUserRP.setForeground(Color.white);
		removeUserRP.setFocusPainted(false);
		removeUserRP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, bloodLake));
		removeUserRP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		removeUserRP.addActionListener(this);







		
		
		
		/*eNameLabel = new JLabel("Employee Name : ");
		eNameLabel.setBounds(150, 120, 200, 40);
		//userNameLabelRP.setBounds(150,50,150,40);
		eNameLabel.setFont(f2);
		eNameLabel.setOpaque(true);
		eNameLabel.setBackground(Color.white);
		eNameLabel.setForeground(kasper);
		removePanel.add(eNameLabel);
		
		eNameTF = new JTextField();
		eNameTF.setBounds(350, 120, 200, 40);
		removePanel.add(eNameTF);
		
		phoneLabel = new JLabel("Phone No. : ");
		phoneLabel.setBounds(250, 250, 120, 30);
		removePanel.add(phoneLabel);
		
		phoneTF = new JTextField();
		phoneTF.setBounds(400, 250, 120, 30);
		phoneTF.setEnabled(false);
		removePanel.add(phoneTF);

		
		roleLabel = new JLabel("Role : ");
		roleLabel.setBounds(250, 300, 120, 30);
		removePanel.add(roleLabel);
		
		roleTF = new JTextField();
		roleTF.setBounds(400, 300, 120, 30);
		removePanel.add(roleTF);
		
		salaryLabel = new JLabel("Salary : ");
		salaryLabel.setBounds(250, 350, 120, 30);
		removePanel.add(salaryLabel);
		
		salaryTF = new JTextField();
		salaryTF.setBounds(400, 350, 120, 30);
		removePanel.add(salaryTF);
		*/










		removePanel.add(findUserRP);
		removePanel.add(userNameTFRP);
		removePanel.add(userNameLabelRP);

		//clickedBar.add(removePanel);
	}

	public void actionPerformed(ActionEvent ae)
	{
		if(tempButton==(JButton)ae.getSource())
		{
			return;
		}
		if(tempButton==null)
		{
			tempButton=(JButton)ae.getSource();
		}

		if(		(JButton)ae.getSource()==logoutButton		)
		{
			lf.setVisible(true);
			this.setVisible(false);
		}
		if(		(JButton)ae.getSource()==myProfileBtn		)
		{
			defaultMethod();
		}

		if(		(JButton)ae.getSource()==viewProfiles		)
		{
			

			for(int i=0; i<profileButtonsArray.length; i++)
			{
				
				if(profileButtonsArray[i]==viewProfiles)
				{
					profileButtonsArray[i].setBackground(kasper);
					profileButtonsArray[i].setForeground(Color.white);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.white));
				}
				else
				{
					profileButtonsArray[i].setBackground(Color.white);
					profileButtonsArray[i].setForeground(kasper);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
				}
			}
			
			initViewPanel();
			clickedBar.removeAll();
			clickedBar.add(viewPanel);			
			clickedBar.repaint();
			tempButton=viewProfiles;



			
		}
		if(		(JButton)ae.getSource()==addProfiles		)
		{
			
			for(int i=0; i<profileButtonsArray.length; i++)
			{
				
				if(profileButtonsArray[i]==addProfiles)
				{
					profileButtonsArray[i].setBackground(kasper);
					profileButtonsArray[i].setForeground(Color.white);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.white));
				}
				else
				{
					profileButtonsArray[i].setBackground(Color.white);
					profileButtonsArray[i].setForeground(kasper);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
				}
			}
			
			initAddPanel();
			clickedBar.removeAll();
			clickedBar.add(addPanel);
			tempButton=(JButton)ae.getSource();
			clickedBar.repaint();
			
		}
		if(		(JButton)ae.getSource()==updateProfiles		)
		{
			
			for(int i=0; i<profileButtonsArray.length; i++)
			{
				
				if(profileButtonsArray[i]==updateProfiles)
				{
					profileButtonsArray[i].setBackground(kasper);
					profileButtonsArray[i].setForeground(Color.white);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(0 ,0, 0, 0, Color.white));
				}
				else
				{
					profileButtonsArray[i].setBackground(Color.white);
					profileButtonsArray[i].setForeground(kasper);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
				}
			}
			
			initUpdatePanel();
			clickedBar.removeAll();
			clickedBar.add(updatePanel);
			tempButton=(JButton)ae.getSource();
			clickedBar.repaint();
		}
		if(		(JButton)ae.getSource()==removeProfiles		)
		{
			
			for(int i=0; i<profileButtonsArray.length; i++)
			{
				
				if(profileButtonsArray[i]==removeProfiles)
				{
					profileButtonsArray[i].setBackground(kasper);
					profileButtonsArray[i].setForeground(Color.white);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.white));
				}
				else
				{
					profileButtonsArray[i].setBackground(Color.white);
					profileButtonsArray[i].setForeground(kasper);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
				}
			}
			
			initRemovePanel();

			clickedBar.removeAll();
			clickedBar.add(removePanel);
			clickedBar.repaint();
			tempButton=(JButton)ae.getSource();
		}

		if((JButton)ae.getSource()==findUserRP)
		{
			if(flagRP==1)
			{
				removePanel.remove(tableScrollPane);
				removePanel.remove(removeUserRP);
				removePanel.repaint();
				flagRP=0;
			}
			findInfoDB();
		}
		if((JButton)ae.getSource()==removeUserRP)
		{
			
			
			removeInfoDB();
			removePanel.remove(tableScrollPane);
			removePanel.remove(removeUserRP);
			removePanel.repaint();
			//flagRP=0;
			
			//findInfoDB();
		}
		if((JButton)ae.getSource()==addBtnAP)
		{
			
			addInfoDB();
		}

		if((JButton)ae.getSource()==autoPassBtnAP)
		{
			Random r = new Random();
			passTFAP.setText(r.nextInt(899999)+100000+"");
			autoPassBtnAP.setEnabled(false);
		}

		if((JButton)ae.getSource()==loadUserUP)
		{
			if(flagUP==1)
			{
				updatePanel.repaint();
				flagUP=0;
			}
			loadFromDB();
		}
		if((JButton)ae.getSource()==updateBtnUP)
		{
			updateInDB();
		}

		if((JButton)ae.getSource()==clearBtnAP)
		{
			clearAP();
		}
		if((JButton)ae.getSource()==clearBtnUP)
		{
			clearUP();
		}
		if((JButton)ae.getSource()==updateBtnMP)
		{
			updateMyDB();
		}
		
		/*if((JPasswordField)ae.getSource()==passPFMP)
		{
			passPFMP.setEchoChar((char) 0);
		}*/

		if((JButton)ae.getSource()==clearBtnMP)
		{
			clearMP();
		}




		
	}

	public void focusGained(FocusEvent fe)
	{
		passPFMP.setEchoChar((char) 0);
	}
	public void focusLost(FocusEvent fe)
	{
		passPFMP.setEchoChar('*');
	}

	public void clearMP()
	{
		myInfo();
		logPortion();
		//defaultMethod();
	}

	public void clearAP()
	{
		userTFAP.setText("");
		passTFAP.setText("");
		eNameTFAP.setText("");
		phoneTFAP.setText("");
		salaryTFAP.setText("");
		autoPassBtnAP.setEnabled(true);
	}

	public void clearUP()
	{
		userTFUP.setText("");
		eNameTFUP.setText("");
		phoneTFUP.setText("");
		salaryTFUP.setText("");
		roleTFUP.setText("");
	}

	public void loadFromDB()
	{
		String loadId = userTFUP.getText();
		tempID=loadId;
		String query = "SELECT `employeeName`, `phoneNumber`, `role`, `salary` FROM `employee` WHERE `userName`='"+loadId+"';";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;
			String eName = null;
			String phnNo = null;
			String role = null;
			double salary = 0.0;			
			while(rs.next())
			{
                eName = rs.getString("employeeName");
				phnNo = rs.getString("phoneNumber");
				role = rs.getString("role");
				salary = rs.getDouble("salary");
				flag=true;
				
				eNameTFUP.setText(eName);
				phoneTFUP.setText(phnNo);
				roleTFUP.setText(role);
				salaryTFUP.setText(""+salary);
				//userTFUP.setEnabled(false);
				updateBtnUP.setEnabled(true);
				flagUP=1;
				//delBtn.setEnabled(true);
			}
			if(!flag)
			{
				eNameTFUP.setText("");
				phoneTFUP.setText("");
				roleTFUP.setText("");
				salaryTFUP.setText("");
				JOptionPane.showMessageDialog(this,"Invalid ID"); 
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }
	}

	public void updateInDB()
	{
		
		String newId;
		String eName;
		String phnNo;
		String role;
		int status=-1;
		double salary;

		try
		{
			newId = userTFUP.getText();
			eName = eNameTFUP.getText();
			phnNo = phoneTFUP.getText();
			role = roleTFUP.getText();
			salary=Double.parseDouble(salaryTFUP.getText());
			if(role.equals("Manager"))
			{
				status =0;
			}
			else if(role.equals("Receptionist"))
			{
				status =1;
			}
			else if(role.equals("Doctor"))
			{
				status =2;
			}
			else if(role.equals("Nurse"))
			{
				status =3;
			}
			if(status==-1)
			{
				JOptionPane.showMessageDialog(this, "Plase check Role again");

				return;
			}

		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(this, "Plase check inserted iformations again");
			return;
		}
		String query = "UPDATE employee SET employeeName='"+eName+"', phoneNumber = '"+phnNo+"', role = '"+role+"', salary = "+salary+" WHERE userName='"+newId+"'";
		System.out.println(status);
		String query2 = "UPDATE Login SET status="+status+" WHERE userName='"+newId+"'";
		//'"+newId+"',"+status+"' WHERE userName=;";
		
        Connection con=null;//for connection
        Statement st = null;//for query execution
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			st = con.createStatement();//create statement
			st.executeUpdate(query);
			st.executeUpdate(query2);
			st.close();
			con.close();

			JOptionPane.showMessageDialog(this, "Account Updated Successfully");
			tempID=newId;
			
			//updateBtnUP.setEnabled(false);
			userTFUP.setText("");
			eNameTFUP.setText("");
			phoneTFUP.setText("");
			roleTFUP.setText("");
			salaryTFUP.setText("");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(this, "Sorry! Something Went Wrong");
		}
	}

	public void addInfoDB()
	{
		
		String newId;
		String newPass;
		String eName;
		String phnNo;
		String role;
		double salary;
		int status = -1;

		try
		{
			newId = userTFAP.getText();
			newPass = passTFAP.getText();
			eName = eNameTFAP.getText();
			phnNo = phoneTFAP.getText();
			role = roleComboAP.getSelectedItem().toString();
			salary = Double.parseDouble(salaryTFAP.getText());
			if(role.equals("Manager"))
			{
				status =0;
			}
			else if(role.equals("Receptionist"))
			{
				status =1;
			}
			else if(role.equals("Doctor"))
			{
				status =2;
			}
			else if(role.equals("Nurse"))
			{
				status =3;
			}
			if(status==-1)
			{
				JOptionPane.showMessageDialog(this, "Plase check Role again");
				return;
			}
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(this, "Plase check inserted iformations again");
			return;
		}

		
		
		String query1 = "INSERT INTO Employee VALUES ('"+newId+"','"+eName+"','"+ phnNo+"','"+role+"',"+salary+");";
		String query2 = "INSERT INTO Login VALUES ('"+newId+"','"+newPass+"',"+status+");";
		System.out.println(query1);
		System.out.println(query2);
        
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03", "root", "");
			Statement stm = con.createStatement();
			stm.execute(query1);
			stm.execute(query2);
			stm.close();
			con.close();
			JOptionPane.showMessageDialog(this, "Acount Created Successfully");

			
			userTFAP.setText("");
			eNameTFAP.setText("");
			phoneTFAP.setText("");
			passTFAP.setText("");
			salaryTFAP.setText("");
			autoPassBtnAP.setEnabled(true);
		}
        catch(Exception ex)
		{
			
			System.out.println(ex.getMessage());
			JOptionPane.showMessageDialog(this, "Sorry! Something Went Wrong");
        }
    }


	public void findInfoDB()
	{
		String loadId = userNameTFRP.getText();
		String query = "SELECT `employeeName`, `phoneNumber`, `role`, `salary` FROM `employee` WHERE `userName`='"+loadId+"';";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;
			String eName = null;
			String phnNo = null;
			String role = null;
			double salary = 0.0;			
			while(rs.next())
			{
                eName = rs.getString("employeeName");
				phnNo = rs.getString("phoneNumber");
				role = rs.getString("role");
				salary = rs.getDouble("salary");
				flag=true;



				String [][]row = {{eName, phnNo,role, ""+salary}};
				String []col = {"Name", "Phone no.", "Role", "Salary"};
				myTable = new JTable(row,col){
				    DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

				    { // initializer block
				        renderRight.setHorizontalAlignment(SwingConstants.CENTER);
				    }

				    @Override
				    public TableCellRenderer getCellRenderer (int arg0, int arg1) {
				        return renderRight;
				    };
				};
				tableScrollPane = new JScrollPane(myTable);
				tableScrollPane.setBounds(50,150,600+100+100+48,68);
				myTable.setFont(f1);
				myTable.setOpaque(false);
				myTable.setForeground(kasper);
				myTable.setBackground(Color.white);
				myTable.setRowHeight(myTable.getRowHeight()+30);
				//myTable.setHorizontalAlignment(SwingConstants.CENTER);

				//DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
				//rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
				//myTable.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
				
				removePanel.add(removeUserRP);
				removePanel.add(tableScrollPane);
				removePanel.repaint();
				flagRP=1;


				



				
				/*eNameTF.setText(eName);
				phoneTF.setText(phnNo);
				roleTF.setText(role);
				salaryTF.setText(""+salary);
				userTF.setEnabled(false);
				updateBtn.setEnabled(true);
				delBtn.setEnabled(true);
				*/
			}
			if(!flag)
			{
				JOptionPane.showMessageDialog(this,"Invalid ID"); 
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }
	}


	public void removeInfoDB()
	{
		String newId = userNameTFRP.getText();
		String query1 = "DELETE from employee WHERE userName='"+newId+"';";

		String query2 = "DELETE from login WHERE userName='"+newId+"';";
		System.out.println(query1);
		//System.out.println(query2);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03", "root", "");
			Statement stm = con.createStatement();
			stm.execute(query1);
			stm.execute(query2);
			stm.close();
			con.close();
			JOptionPane.showMessageDialog(this, "Success !!!");
			
		}
        catch(Exception ex)
		{
			JOptionPane.showMessageDialog(this, "Oops !!!");
        }
	}

	public void defaultMethod()
	{
		for(int i=0; i<profileButtonsArray.length; i++)
			{
				
				if(profileButtonsArray[i]==myProfileBtn)
				{
					profileButtonsArray[i].setBackground(kasper);
					profileButtonsArray[i].setForeground(Color.white);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.white));
				}
				else
				{
					profileButtonsArray[i].setBackground(Color.white);
					profileButtonsArray[i].setForeground(kasper);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
				}
			}
			

			initMyPanel();
			clickedBar.removeAll();
			clickedBar.add(myPanel);
			clickedBar.repaint();
			tempButton=myProfileBtn;
	}

	public static void main(String args[])
	{
		AdminFrame af = new AdminFrame(new LoginFrame(), "Nafiz");
		af.setVisible(true);
	}

}
