import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.sql.*;

public class LoginFrame extends JFrame implements ActionListener
{
	ImageIcon backGroundIcon = new ImageIcon("res/docback6.png");
	ImageIcon loginPageIcon = new ImageIcon("res/login2.png");
	JLabel backGroundLabel = new JLabel(backGroundIcon) ;
	JLabel loginPageLabel = new JLabel(loginPageIcon) ;

	Font f1 = new Font("Calibri",Font.PLAIN,15);
	Font f2 = new Font("Calibri",Font.PLAIN,20);

	Color kasper = new Color(0,167,157);


	JPanel loginPanel, adminPanel;

	JLabel userNameLabel, passLabel;

	JTextField userNameTF;

	JPasswordField passPF;

	JButton loginButton, backButton = new JButton("BACK");


	public LoginFrame()
	{
		super("Hospital Management System");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1120,630);
		setLocationRelativeTo(null);

		backGroundLabel.setBounds(0,0,1120,630);
		//loginPageLabel.setBounds(0,0,550,570);


		initLoginPanel();

		
		
		//add(adminPanel);

		

	}

	public void initLoginPanel()
	{
		

		loginPanel = new JPanel();
		loginPanel.setLayout(null);

		
		userNameLabel = new JLabel("   User Name");
		userNameLabel.setOpaque(true);
		userNameLabel.setBackground(Color.white);
		userNameLabel.setBounds(350,180,130,40);
		userNameLabel.setFont(f1);
		//userNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		userNameLabel.setForeground(kasper);


		userNameTF = new JTextField();
		userNameTF.setBounds(450,180,280+20,40);
		userNameTF.setFont(f2);
		userNameTF.setBorder(BorderFactory.createEmptyBorder());
		//userNameTF.setHorizontalAlignment(SwingConstants.RIGHT);
		userNameTF.setForeground(Color.GRAY);
		userNameTF.addActionListener(this);


		passLabel = new JLabel("   Password");
		passLabel.setOpaque(true);
		passLabel.setBackground(Color.white);
		passLabel.setBounds(350,240,130,40);
		passLabel.setFont(f1);
		//passLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		passLabel.setForeground(kasper);

		passPF = new JPasswordField();
		passPF.setBounds(450,240,280+20,40);
		passPF.setFont(f2);
		passPF.setBorder(BorderFactory.createEmptyBorder());
		passPF.setForeground(Color.GRAY);
		//passPF.setHorizontalAlignment(SwingConstants.RIGHT);


		loginButton = new JButton("LOGIN");
		loginButton.setBounds(350,300,380+20,40);
		loginButton.setFont(f1);
		loginButton.setForeground(Color.gray);
		loginButton.setBackground(Color.white);
		loginButton.setForeground(Color.gray);
		loginButton.setFocusPainted(false);
		loginButton.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		loginButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		loginButton.addActionListener(this);


		backButton.setBounds(350,360,380+20,40);
		backButton.setFont(f1);
		backButton.setForeground(Color.gray);
		backButton.setBackground(Color.white);
		backButton.setForeground(Color.gray);
		backButton.setFocusPainted(false);
		backButton.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		backButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		backButton.addActionListener(this);





		loginPanel.add(userNameTF);
		loginPanel.add(userNameLabel);

		loginPanel.add(passPF);
		loginPanel.add(passLabel);

		loginPanel.add(loginButton);
		loginPanel.add(backButton);
		//loginPanel.add(loginPageLabel);
		loginPanel.add(backGroundLabel);


		add(loginPanel);

	}

	public void initAdminPanel()
	{
		adminPanel = new JPanel();
		adminPanel.setLayout(null);

		backButton.setBounds(200,200,100,100);
		backButton.addActionListener(this);

		adminPanel.add(backButton);

		adminPanel.add(backGroundLabel);
		//adminPanel.setVisible(false);
		add(adminPanel);

	}


	public void actionPerformed(ActionEvent ae)
	{
		
		if(		(JButton)ae.getSource()==loginButton		)
		{
			detectUser();
		}

		if(		(JButton)ae.getSource()==backButton		)
		{
			this.setVisible(false);
			Start s = new Start();
			this.setVisible(false);
			s.setVisible(true);
		}

			
	}

	public void detectUser()
	{
		String query = "SELECT `userName`, `password`, `status` FROM `login`;";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;			
			while(rs.next())
			{
                String userName = rs.getString("userName");
				String password = rs.getString("password");
				int status = rs.getInt("status");
				
				if(userName.equals(userNameTF.getText()) && password.equals(passPF.getText()))
				{
					flag=true;
					if(status==0)
					{
						//String name = nameFetch();
						//System.out.println(name);
						AdminFrame af =  new AdminFrame(this,userName);
						af.setVisible(true);
						clearLog();
						this.setVisible(false);

					}
					else if(status==1)
					{
						//String name=nameFetch();
						RecepFrame rf =  new RecepFrame(this,userName);
						rf.setVisible(true);
						clearLog();
						this.setVisible(false);

					}
					else if(status==2)
					{
						//String name=nameFetch();
						DocFrame df =  new DocFrame(this,userName);
						df.setVisible(true);
						clearLog();
						this.setVisible(false);

					}
					else if(status==3)
					{
						//String name=nameFetch();
						SisFrame sf =  new SisFrame(this,userName);
						sf.setVisible(true);
						clearLog();
						this.setVisible(false);

					}
					
					else{}
				}
			}
			if(!flag)
			{
				JOptionPane.showMessageDialog(this,"Invalid ID or Password"); 
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }
	}

	public void clearLog()
	{
		userNameTF.setText("");
		passPF.setText("");
	}




	public static void main(String[] args)
	{
		LoginFrame logFrame = new LoginFrame();
		logFrame.setVisible(true);
	}
}