import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.*;
import java.awt.*;
import java.sql.*;
import java.util.*;


public class DocFrame extends JFrame implements ActionListener,FocusListener

{
	LoginFrame lf;
	String currentUserName;
	ImageIcon backGroundIcon = new ImageIcon("res/docback6.png");
	JLabel backGroundLabel = new JLabel(backGroundIcon) ;

	Font f1 = new Font("Calibri",Font.PLAIN,15);
	Font f2 = new Font("Calibri",Font.PLAIN,20);
	Font f3 = new Font("Calibri",Font.PLAIN,25);
	Font f4 = new Font("Calibri",Font.BOLD,35);




	Color kasper = new Color(0,167,157);
	Color bloodLake = new Color(220,90,73);


	JPanel  sideBar, clickedBar;

	JPanel treatPanel,addPanel, updatePanel,viewPanel,myPanel;
	JButton backButton = new JButton("BACK"),tempButton;
	JLayeredPane adminPanel;


	JButton viewPatients, manageProfiles ,logoutButton, addPatients, updatePatients, treatPatients,myProfileBtn;

	JButton []profileButtonsArray = new JButton[3];

	JLabel adminHeader, currentUserLabel;


	JLabel patientIDLabelTP;
	JButton removePatientTP,findPatientTP;
	JTextField patientIDTFTP;

	
	JTextField patientIDTFAP, phoneTFAP, pNameTFAP, salaryTFAP, pAgeTFAP;
	JLabel patientIDLabelAP, pNameLabelAP, phoneLabelAP, genderLabelAP, salaryLabelAP, pAgeLabelAP,bloodLabelAP;

	JTable myTable;
	JScrollPane tableScrollPane;

	int flagRP=0,temp=-1,temp2=-1;

	JComboBox genderComboAP, bloodComboAP;
	JButton updateBtnTP, clearBtnTP;




	JLabel patientIDLabelUP, pNameLabelUP, phoneLabelUP, genderLabelUP, salaryLabelUP, pAgeLabelUP,bloodLabelUP;
	JTextField patientIDTFUP, phoneTFUP, pNameTFUP, salaryTFUP, pAgeTFUP;
	JButton loadPatientUP,updateBtnUP, clearBtnUP;
	String tempID;
	int flagUP=0;
	JComboBox genderComboUP, bloodComboUP;
	JButton addBtnUP;


	JTable tTable;
	JScrollPane tScroll;

	JTextArea instructionTATP;
	JScrollPane instructionSPTP;

	JComboBox releasePermitCombo;
	JLabel releasePermitLabelTP;


	JLabel userLabelMP, eNameLabelMP,phoneLabelMP,passLabelMP;
	JTextField userTFMP, eNameTFMP, phoneTFMP;
	JButton clearBtnMP, updateBtnMP;
	JPasswordField passPFMP;





	public DocFrame(LoginFrame lf, String currentUserName)
	{
		super("Hospital Management System");
		this.lf=lf;
		this.currentUserName=currentUserName;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1120,630);
		setLocationRelativeTo(null);

		backGroundLabel.setBounds(0,0,1120,630);

		

		initAdminPanel();
		//initViewPanel();
		//initAddPanel();
		//initTreatmentPanel();
		//initUpdatePanel();
		//clickedBar.add(treatPanel);
		defaultMethod();

	}














	public void initMyPanel()
	{
		myPanel = new JPanel();
		myPanel.setLayout(null);
		myPanel.setOpaque(false);







		userLabelMP = new JLabel("   User Name");
		userLabelMP.setBounds(150,40,120,40);
		userLabelMP.setFont(f1);
		userLabelMP.setOpaque(true);
		userLabelMP.setBackground(Color.white);
		userLabelMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		userLabelMP.setHorizontalAlignment(SwingConstants.LEFT);
		userLabelMP.setForeground(kasper);
		myPanel.add(userLabelMP);
		
		userTFMP = new JTextField(currentUserName);
		userTFMP.setBounds(270,40,490,40);
		userTFMP.setFont(f2);
		userTFMP.setBackground(Color.white);
		userTFMP.setForeground(Color.gray);
		userTFMP.setBorder(BorderFactory.createEmptyBorder());
		userTFMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		userTFMP.setHorizontalAlignment(SwingConstants.LEFT);
		userTFMP.setEditable(false);
		myPanel.add(userTFMP);

		
		eNameLabelMP = new JLabel("   Employee Name");
		eNameLabelMP.setBounds(150,100,120,40);
		eNameLabelMP.setFont(f1);
		eNameLabelMP.setOpaque(true);
		eNameLabelMP.setBackground(Color.white);
		eNameLabelMP.setForeground(kasper);
		eNameLabelMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		myPanel.add(eNameLabelMP);
		
		eNameTFMP = new JTextField();
		eNameTFMP.setBounds(270,100,490,40);
		eNameTFMP.setFont(f2);
		eNameTFMP.setBackground(Color.white);
		eNameTFMP.setForeground(Color.GRAY);
		eNameTFMP.setBorder(BorderFactory.createEmptyBorder());
		eNameTFMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		eNameTFMP.setHorizontalAlignment(SwingConstants.LEFT);
		myPanel.add(eNameTFMP);
		
		phoneLabelMP = new JLabel("   Phone");
		phoneLabelMP.setBounds(150,160,120,40);
		phoneLabelMP.setFont(f1);
		phoneLabelMP.setOpaque(true);
		phoneLabelMP.setBackground(Color.white);
		phoneLabelMP.setForeground(kasper);
		phoneLabelMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		myPanel.add(phoneLabelMP);
		
		phoneTFMP = new JTextField();
		phoneTFMP.setBounds(270,160,490,40);
		phoneTFMP.setFont(f2);
		//phoneTFUP.setOpaque(false);
		phoneTFMP.setBackground(Color.white);
		phoneTFMP.setForeground(Color.GRAY);
		phoneTFMP.setBorder(BorderFactory.createEmptyBorder());
		phoneTFMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		phoneTFMP.setHorizontalAlignment(SwingConstants.LEFT);
		myPanel.add(phoneTFMP);



		passLabelMP = new JLabel("   Password");
		passLabelMP.setBounds(150,220,120,40);
		passLabelMP.setFont(f1);
		passLabelMP.setOpaque(true);
		passLabelMP.setBackground(Color.white);
		passLabelMP.setForeground(kasper);
		passLabelMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		myPanel.add(passLabelMP);
		
		passPFMP = new JPasswordField();
		passPFMP.setBounds(270,220,490,40);
		passPFMP.setFont(f2);
		//phoneTFUP.setOpaque(false);
		passPFMP.setBackground(Color.white);
		passPFMP.setForeground(Color.GRAY);
		passPFMP.setBorder(BorderFactory.createEmptyBorder());
		passPFMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		passPFMP.setHorizontalAlignment(SwingConstants.LEFT);
		passPFMP.addFocusListener(this);
		myPanel.add(passPFMP);

		
		clearBtnMP = new JButton("Clear");
		clearBtnMP.setBounds(150, 300, 305, 40);
		clearBtnMP.setFont(f1);
		clearBtnMP.setForeground(Color.gray);
		clearBtnMP.setBackground(Color.white);
		clearBtnMP.setFocusPainted(false);
		clearBtnMP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		clearBtnMP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		clearBtnMP.addActionListener(this);
		myPanel.add(clearBtnMP);
		


		updateBtnMP = new JButton("Update");
		updateBtnMP.setBounds(455, 300, 305, 40);
		updateBtnMP.setFont(f1);
		updateBtnMP.setForeground(kasper);
		updateBtnMP.setBackground(Color.white);
		updateBtnMP.setFocusPainted(false);
		updateBtnMP.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, kasper));
		updateBtnMP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		updateBtnMP.addActionListener(this);
		myPanel.add(updateBtnMP);


		myInfo();
		logPortion();
	}

	public void myInfo()
	{

		String loadId =userTFMP.getText();// userTFMP.getText();
		///tempID=loadId;
		String query = "SELECT `employeeName`, `phoneNumber` FROM `employee` WHERE `userName`='"+loadId+"';";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;
			String eName = null;
			String phnNo = null;
					
			while(rs.next())
			{
                eName = rs.getString("employeeName");
				phnNo = rs.getString("phoneNumber");
				flag=true;
				
				eNameTFMP.setText(eName);
				currentUserLabel.setText(eName);
				currentUserLabel.repaint();
				phoneTFMP.setText(phnNo);
				
				updateBtnMP.setEnabled(true);
				flagUP=1;

				logPortion();
				//delBtn.setEnabled(true);
			}
			if(!flag)
			{
				eNameTFMP.setText("");
				phoneTFMP.setText("");
				updateBtnMP.setEnabled(false);
				

				JOptionPane.showMessageDialog(this,"Invalid ID"); 
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }


	}


	public void logPortion()
	{


		String loadId =userTFMP.getText();
		///tempID=loadId;
		String query = "SELECT `Password` FROM `login` WHERE `userName`='"+loadId+"';";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;
			String pass = null;
					
			while(rs.next())
			{
                pass = rs.getString("Password");
				flag=true;
				
				passPFMP.setText(pass);
				
				updateBtnMP.setEnabled(true);
				flagUP=1;
				//delBtn.setEnabled(true);
			}
			if(!flag)
			{
				eNameTFMP.setText("");
				phoneTFMP.setText("");
				updateBtnMP.setEnabled(false);
				

				JOptionPane.showMessageDialog(this,"Invalid ID"); 
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }

	}









	public void updateMyDB()
	{
		
		String newId;
		String eName;
		String phnNo;
		String pass;

		try
		{
			newId = userTFMP.getText();
			eName = eNameTFMP.getText();
			phnNo = phoneTFMP.getText();
			pass =  passPFMP.getText();

		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(this, "Plase check inserted iformations again");
			return;
		}
		String query = "UPDATE employee SET employeeName='"+eName+"', phoneNumber = '"+phnNo+"' WHERE userName='"+newId+"'";
		//System.out.println(status);
		String query2 = "UPDATE Login SET Password="+pass+" WHERE userName='"+newId+"'";
		
        Connection con=null;//for connection
        Statement st = null;//for query execution
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			st = con.createStatement();//create statement
			st.executeUpdate(query);
			st.executeUpdate(query2);
			st.close();
			con.close();

			currentUserLabel.setText(eName);

			currentUserLabel.repaint();

			JOptionPane.showMessageDialog(this, "Account Updated Successfully");
			
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(this, "Sorry! Something Went Wrong");
		}
	}


















	public void initAdminPanel()
	{
		adminPanel = new JLayeredPane();
		adminPanel.setLayout(null);

		sideBar = new JPanel();
		sideBar.setLayout(null);
		sideBar.setBounds(0,50,150,580);
		sideBar.setOpaque(false);
		

		clickedBar = new JPanel();
		clickedBar.setLayout(new GridLayout(1,1));
		clickedBar.setBounds(150,50,970,580);
		clickedBar.setOpaque(false);
		//clickedBar.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));


		backButton.setBounds(500,200,100,100);
		backButton.addActionListener(this);

		adminHeader = new JLabel(" Doctor's Desk");
		adminHeader.setBounds(0,0,1120,70);
		adminHeader.setFont(f4);
		adminHeader.setOpaque(true);
		adminHeader.setForeground(kasper);
		adminHeader.setBackground(Color.white);
		//adminHeader.setForeground(Color.white);
		//adminHeader.setFocusPainted(false);
		adminHeader.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, kasper));
		adminHeader.setVerticalAlignment(JLabel.CENTER);
		adminHeader.setHorizontalAlignment(JLabel.LEFT);

		currentUserLabel = new JLabel(currentUserName);
		currentUserLabel.setBounds(300,15,682,40);
		currentUserLabel.setFont(f2);
		//currentUserNameLabel.setOpaque(true);
		currentUserLabel.setForeground(Color.gray);
		currentUserLabel.setBackground(Color.gray);
		currentUserLabel.setVerticalAlignment(JLabel.CENTER);
		currentUserLabel.setHorizontalAlignment(JLabel.RIGHT);


		logoutButton = new JButton("Logout");
		logoutButton.setBounds(997,15,100,40);
		logoutButton.setFont(f2);
		logoutButton.setForeground(bloodLake);
		logoutButton.setBackground(Color.white);
		logoutButton.setFocusPainted(false);
		logoutButton.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.gray));
		logoutButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		logoutButton.addActionListener(this);


		myProfileBtn = new JButton("My Profile");
		myProfileBtn.setBounds(0,40,150,40);
		myProfileBtn.setFont(f1);
		myProfileBtn.setForeground(Color.gray);
		myProfileBtn.setBackground(Color.white);
		myProfileBtn.setForeground(Color.gray);
		myProfileBtn.setFocusPainted(false);
		myProfileBtn.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		myProfileBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		myProfileBtn.addActionListener(this);
		
		viewPatients = new JButton("View Patients");
		viewPatients.setBounds(0,40+40,150,40);
		viewPatients.setFont(f1);
		viewPatients.setForeground(Color.gray);
		viewPatients.setBackground(Color.white);
		viewPatients.setForeground(Color.gray);
		viewPatients.setFocusPainted(false);
		viewPatients.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		viewPatients.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		viewPatients.addActionListener(this);

		treatPatients = new JButton("Treatment");
		treatPatients.setBounds(0,80+40,150,40);
		treatPatients.setFont(f1);
		treatPatients.setForeground(Color.gray);
		treatPatients.setBackground(Color.white);
		treatPatients.setForeground(Color.gray);
		treatPatients.setFocusPainted(false);
		treatPatients.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		treatPatients.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		treatPatients.addActionListener(this);


		profileButtonsArray[0]=myProfileBtn;

		profileButtonsArray[1]=viewPatients;
		
		profileButtonsArray[2]=treatPatients;

		

		sideBar.add(myProfileBtn);
		sideBar.add(viewPatients);
		
		sideBar.add(treatPatients);

		adminPanel.add(sideBar);
		adminPanel.add(backGroundLabel, Integer.valueOf(1));

    	adminPanel.add(logoutButton, Integer.valueOf(4));
    	adminPanel.add(currentUserLabel,Integer.valueOf(3));
    	adminPanel.add(adminHeader, Integer.valueOf(2));
    	adminPanel.add(sideBar, Integer.valueOf(5));
    	adminPanel.add(clickedBar, Integer.valueOf(6));


		//adminPanel.add(backGroundLabel);
		add(adminPanel);
	}

	public void initViewPanel()
	{

		viewPanel = new JPanel();
		viewPanel.setLayout(null);
		viewPanel.setOpaque(false);



		String query = "SELECT *  FROM `Patient`;"; 
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try 
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");


			
			int c=0;
			String[][] data = new String[100][7];
			String column[] = {"Patient ID", "Patient Name", "Age","Blood Group","Gender","Phone No.", "Release Permit"};

			int j,z=0;
			for(int i = 0; rs.next(); i++){
				
				
				j = 0;
				data[i][j++] = rs.getString("patientID");
				data[i][j++] = rs.getString("patientName");
				data[i][j++] = rs.getString("patientAge");
				data[i][j++] = rs.getString("bloodGroup");
				data[i][j++] = rs.getString("Gender");
				data[i][j++] = rs.getString("phoneNumber");
				data[i][j++] = rs.getString("releasePermit");

				c++;
				z=z+40;
			}

			String [][] newData = new String[c][7];
			for(int i=0; i<c; i++)
			{
				for(int j2=0; j2<7; j2++)
				{
					newData[i][j2]=data[i][j2];
				}
			    
			}


			  

			tTable = new JTable(newData, column){
			    DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

			    { 
			        renderRight.setHorizontalAlignment(SwingConstants.CENTER);
			    }

			    @Override
			    public TableCellRenderer getCellRenderer (int arg0, int arg1) {
			        return renderRight;
			    };
			};
			
			tTable.setFont(f1);
			tTable.setOpaque(false);
			tTable.setForeground(Color.GRAY);
			tTable.setBackground(Color.white);
			tTable.setRowHeight(tTable.getRowHeight()+20);
    		tScroll = new JScrollPane(tTable);
			tScroll.setBounds(50,40,850,450);          

		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }

		
		viewPanel.add(tScroll);
	}


	public void initTreatmentPanel()
	{
		treatPanel = new JPanel();
		treatPanel.setLayout(null);
		treatPanel.setOpaque(false);
		//treatPanel.setBackground(Color.white);

		patientIDLabelTP = new JLabel("   Patient ID");
		patientIDLabelTP.setBounds(50,40,110,40);
		patientIDLabelTP.setFont(f1);
		patientIDLabelTP.setOpaque(true);
		patientIDLabelTP.setBackground(Color.white);
		patientIDLabelTP.setForeground(kasper);

		patientIDTFTP = new JTextField();
		patientIDTFTP.setBounds(160,40,585,40);
		patientIDTFTP.setFont(f2);
		//patientIDTFTP.setOpaque(true);
		patientIDTFTP.setBackground(Color.white);
		patientIDTFTP.setForeground(Color.GRAY);
		patientIDTFTP.setBorder(BorderFactory.createEmptyBorder());
		patientIDTFTP.setHorizontalAlignment(SwingConstants.CENTER);

		findPatientTP = new JButton("Find");
		findPatientTP.setBounds(745,40,150,40);
		findPatientTP.setFont(f1);
		findPatientTP.setForeground(Color.gray);
		findPatientTP.setBackground(Color.white);
		findPatientTP.setFocusPainted(false);
		findPatientTP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		findPatientTP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		findPatientTP.addActionListener(this);

		


		instructionTATP = new JTextArea();
		instructionTATP.setFont(f2);
		instructionTATP.setBackground(Color.white);
		instructionTATP.setForeground(Color.GRAY);
		instructionTATP.setBorder(BorderFactory.createEmptyBorder());
		instructionTATP.setLineWrap(true);
		instructionTATP.setWrapStyleWord(true);

		instructionSPTP = new JScrollPane(instructionTATP);
		instructionSPTP.setBounds(50,180,850,250);


		releasePermitLabelTP = new JLabel("   Release Permit");
		releasePermitLabelTP.setBounds(50,450,110,40);
		releasePermitLabelTP.setFont(f1);
		releasePermitLabelTP.setOpaque(true);
		releasePermitLabelTP.setBackground(Color.white);
		releasePermitLabelTP.setForeground(kasper);


		String items[] = {"no","yes"};
		releasePermitCombo = new JComboBox(items);
		releasePermitCombo.setBounds(160,450,150,40);
		releasePermitCombo.setFont(f1);
		releasePermitCombo.setOpaque(true);
		releasePermitCombo.setBackground(Color.white);
		releasePermitCombo.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.white));
		((JLabel)releasePermitCombo.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
		releasePermitCombo.setForeground(kasper);


		clearBtnTP = new JButton("Clear");
		clearBtnTP.setBounds(600, 450, 150, 40);
		clearBtnTP.setFont(f1);
		clearBtnTP.setForeground(Color.gray);
		clearBtnTP.setBackground(Color.white);
		clearBtnTP.setFocusPainted(false);
		clearBtnTP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		clearBtnTP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		clearBtnTP.addActionListener(this);
		//treatPanel.add(clearBtnTP);



		updateBtnTP = new JButton("Update");
		updateBtnTP.setBounds(750, 450, 150, 40);
		updateBtnTP.setFont(f1);
		updateBtnTP.setForeground(kasper);
		updateBtnTP.setBackground(Color.white);
		updateBtnTP.setFocusPainted(false);
		updateBtnTP.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, kasper));
		updateBtnTP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		updateBtnTP.addActionListener(this);
		//treatPanel.add(updateBtnTP);




		treatPanel.add(findPatientTP);
		treatPanel.add(patientIDTFTP);
		treatPanel.add(patientIDLabelTP);

		//treatPanel.add(instructionSPTP);

		treatPanel.add(releasePermitLabelTP);
		treatPanel.add(releasePermitCombo);
		releasePermitCombo.setVisible(false);
		releasePermitLabelTP.setVisible(false);

	}

	public void actionPerformed(ActionEvent ae)
	{
		if(tempButton==(JButton)ae.getSource())
		{
			return;
		}
		if(tempButton==null)
		{
			tempButton=(JButton)ae.getSource();
		}
		

		if(		(JButton)ae.getSource()==logoutButton		)
		{
			lf.setVisible(true);
			this.setVisible(false);
		}
		if(		(JButton)ae.getSource()==myProfileBtn		)
		{
			defaultMethod();
		}
		if(		(JButton)ae.getSource()==viewPatients		)
		{
			
			for(int i=0; i<profileButtonsArray.length; i++)
			{
				
				if(profileButtonsArray[i]==viewPatients)
				{
					profileButtonsArray[i].setBackground(kasper);
					profileButtonsArray[i].setForeground(Color.white);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.white));
				}
				else
				{
					profileButtonsArray[i].setBackground(Color.white);
					profileButtonsArray[i].setForeground(kasper);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
				}
			}
			
			
			clickedBar.removeAll();
			initViewPanel();
			clickedBar.add(viewPanel);			
			clickedBar.repaint();
			tempButton=(JButton)ae.getSource();
		}
		
		


		
		if(		(JButton)ae.getSource()==treatPatients		)
		{
			
			for(int i=0; i<profileButtonsArray.length; i++)
			{
				
				if(profileButtonsArray[i]==treatPatients)
				{
					profileButtonsArray[i].setBackground(kasper);
					profileButtonsArray[i].setForeground(Color.white);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.white));
				}
				else
				{
					profileButtonsArray[i].setBackground(Color.white);
					profileButtonsArray[i].setForeground(kasper);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
				}
			}
			
			initTreatmentPanel();

			clickedBar.removeAll();
			clickedBar.add(treatPanel);
			clickedBar.repaint();
			tempButton=(JButton)ae.getSource();
		}

		

		if((JButton)ae.getSource()==findPatientTP)
		{
			if(flagRP==1)
			{
				treatPanel.remove(tableScrollPane);
				treatPanel.remove(instructionSPTP);
				treatPanel.remove(clearBtnTP);
				treatPanel.remove(updateBtnTP);
				//treatPanel.remove(releasePermitLabelTP);
				//treatPanel.remove(releasePermitCombo);
				//treatPanel.remove(removePatientTP);
				treatPanel.repaint();
				flagRP=0;
			}
			findInfoDB();
			loadInstruction();
		}
		
		if((JButton)ae.getSource()==updateBtnTP)
		{
			
			updateInDB();
		}

		

		

		if((JButton)ae.getSource()==clearBtnTP)
		{
			clearTP();
		}

		if((JButton)ae.getSource()==updateBtnMP)
		{
			updateMyDB();
		}


		if((JButton)ae.getSource()==clearBtnMP)
		{
			clearMP();
		}

		



		
	}
	public void focusGained(FocusEvent fe)
	{
		passPFMP.setEchoChar((char) 0);
	}
	public void focusLost(FocusEvent fe)
	{
		passPFMP.setEchoChar('*');
	}


	public void clearMP()
	{
		myInfo();
		logPortion();
		//defaultMethod();
	}




	public void clearTP()
	{
		
		patientIDTFTP.setText("");
		treatPanel.remove(tableScrollPane);
		treatPanel.remove(instructionSPTP);
		
		treatPanel.remove(updateBtnTP);
		treatPanel.remove(clearBtnTP);
		//treatPanel.remove(releasePermitLabelTP);
		//treatPanel.remove(releasePermitCombo);
		releasePermitCombo.setVisible(false);
		releasePermitLabelTP.setVisible(false);
		treatPanel.repaint();
		
	}
	


	
	
	public void updateInDB()
	{
		
		
		String insT = null;
		String newId = null;
		String rPermit = null;

		try
		{
			insT = instructionTATP.getText();
			newId= patientIDTFTP.getText();
			rPermit = releasePermitCombo.getSelectedItem().toString();
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(this, "Plase check inserted iformations again");
			return;
		}
		String query = "UPDATE treatment SET instruction='"+insT+"' WHERE patientID='"+newId+"'";
		String query2 = "UPDATE patient SET releasePermit='"+rPermit+"' WHERE patientID='"+newId+"'";
		
        Connection con=null;//for connection
        Statement st = null;//for query execution
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			st = con.createStatement();//create statement
			st.executeUpdate(query);
			st.executeUpdate(query2);
			st.close();
			con.close();


			treatPanel.remove(tableScrollPane);
			findInfoDB();
			repaint();


			JOptionPane.showMessageDialog(this, "Account Updated Successfully");
			//tempID=newId;
			
			
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(this, "Sorry! Something Went Wrong");
		}
	}

	


	

	


	public void findInfoDB()
	{
		String loadId = patientIDTFTP.getText();
		String query = "SELECT `patientID`, `patientName`, `patientName`, `patientAge`, `bloodGroup`, `Gender`, `phoneNumber`,`releasePermit`  FROM `Patient` WHERE `patientID`='"+loadId+"';";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;

			String pID = null;
			String pName=null;
			String pAge=null;
			String phnNo=null;
			String gender=null;
			String blood=null;
			String rPermit=null;

			while(rs.next())
			{

				pID = rs.getString("patientID");
				pName = rs.getString("patientName");
				pAge = rs.getString("patientAge");
				blood = rs.getString("bloodGroup");
				gender = rs.getString("Gender");
				phnNo = rs.getString("phoneNumber");
				rPermit = rs.getString("releasePermit");

				flag=true;



				String [][]row = {{pID, pName,pAge, blood , gender, phnNo, rPermit}};
				String col[] = {"Patient ID", "Patient Name", "Age","Blood Group","Gender","Phone No.", "Release Permit"};
				myTable = new JTable(row,col){
				    DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

				    { // initializer block
				        renderRight.setHorizontalAlignment(SwingConstants.CENTER);
				    }

				    @Override
				    public TableCellRenderer getCellRenderer (int arg0, int arg1) {
				        return renderRight;
				    };
				};
				tableScrollPane = new JScrollPane(myTable);
				tableScrollPane.setBounds(50,100,600+100+100+47,68);
				myTable.setFont(f1);
				myTable.setOpaque(false);
				myTable.setForeground(Color.gray);
				myTable.setBackground(Color.white);
				myTable.setRowHeight(myTable.getRowHeight()+30);
				
				releasePermitCombo.setSelectedItem(rPermit);

				treatPanel.add(tableScrollPane);
				treatPanel.add(instructionSPTP);
				//treatPanel.add(releasePermitLabelTP);
				//treatPanel.add(releasePermitCombo);
				releasePermitCombo.setVisible(true);
				releasePermitLabelTP.setVisible(true);
				treatPanel.add(clearBtnTP);
				treatPanel.add(updateBtnTP);
				treatPanel.repaint();
				flagRP=1;


			}
			if(!flag)
			{
				
				releasePermitCombo.setVisible(false);
				releasePermitLabelTP.setVisible(false);
				treatPanel.repaint();
				JOptionPane.showMessageDialog(this,"Invalid ID");
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }
	}

	public void loadInstruction()
	{
		String loadId = patientIDTFTP.getText();
        String query = "SELECT `instruction` FROM `treatment` WHERE `patientID`='"+loadId+"';";
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;

			String insT = null;
			

			while(rs.next())
			{

				insT = rs.getString("instruction");
				

				flag=true;


				instructionTATP.setText(insT);
				updateBtnTP.setEnabled(true);
				treatPanel.repaint();
				//flagRP=1;

			}
			if(!flag)
			{
				//JOptionPane.showMessageDialog(this,"Invalid ID"); 
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }
	}

	public void defaultMethod()
	{
		for(int i=0; i<profileButtonsArray.length; i++)
			{
				
				if(profileButtonsArray[i]==myProfileBtn)
				{
					profileButtonsArray[i].setBackground(kasper);
					profileButtonsArray[i].setForeground(Color.white);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.white));
				}
				else
				{
					profileButtonsArray[i].setBackground(Color.white);
					profileButtonsArray[i].setForeground(kasper);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
				}
			}
			

			initMyPanel();
			clickedBar.removeAll();
			clickedBar.add(myPanel);
			clickedBar.repaint();
			tempButton=myProfileBtn;
	}
	


	

	public static void main(String args[])
	{
		DocFrame df = new DocFrame(new LoginFrame(),"doc01");
		df.setVisible(true);
	}

}
