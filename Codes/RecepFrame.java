import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.*;
import java.awt.*;
import java.sql.*;
import java.util.*;


public class RecepFrame extends JFrame implements ActionListener,FocusListener

{
	LoginFrame lf;
	String currentUserName;
	ImageIcon backGroundIcon = new ImageIcon("res/docback6.png");
	JLabel backGroundLabel = new JLabel(backGroundIcon) ;

	Font f1 = new Font("Calibri",Font.PLAIN,15);
	Font f2 = new Font("Calibri",Font.PLAIN,20);
	Font f3 = new Font("Calibri",Font.PLAIN,25);
	Font f4 = new Font("Calibri",Font.BOLD,35);




	Color kasper = new Color(0,167,157);
	Color bloodLake = new Color(220,90,73);


	JPanel  sideBar, clickedBar;

	JPanel removePanel,addPanel, updatePanel,viewPanel,myPanel;
	JButton backButton = new JButton("BACK"),tempButton;
	JLayeredPane adminPanel;


	JButton viewPatients, manageProfiles ,logoutButton, addPatients, updatePatients, removePatients,myProfileBtn;

	JButton []profileButtonsArray = new JButton[5];

	JLabel adminHeader, currentUserLabel;


	JLabel patientIDLabelRP;
	JButton removePatientRP,findPatientRP;
	JTextField patientIDTFRP;

	
	JTextField patientIDTFAP, phoneTFAP, pNameTFAP, salaryTFAP, pAgeTFAP;
	JLabel patientIDLabelAP, pNameLabelAP, phoneLabelAP, genderLabelAP, salaryLabelAP, pAgeLabelAP,bloodLabelAP;

	JTable myTable;
	JScrollPane tableScrollPane;

	int flagRP=0,temp=-1,temp2=-1;

	JComboBox genderComboAP, bloodComboAP;
	JButton addBtnAP, clearBtnAP;




	JLabel patientIDLabelUP, pNameLabelUP, phoneLabelUP, genderLabelUP, salaryLabelUP, pAgeLabelUP,bloodLabelUP;
	JTextField patientIDTFUP, phoneTFUP, pNameTFUP, salaryTFUP, pAgeTFUP;
	JButton loadPatientUP,updateBtnUP, clearBtnUP;
	String tempID;
	int flagUP=0;
	JComboBox genderComboUP, bloodComboUP;
	JButton addBtnUP;


	JTable tTable;
	JScrollPane tScroll;



	JLabel userLabelMP, eNameLabelMP,phoneLabelMP,passLabelMP;
	JTextField userTFMP, eNameTFMP, phoneTFMP;
	JButton clearBtnMP, updateBtnMP;
	JPasswordField passPFMP;





	public RecepFrame(LoginFrame lf, String currentUserName)
	{
		super("Hospital Management System");
		this.lf=lf;
		this.currentUserName=currentUserName;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1120,630);
		setLocationRelativeTo(null);

		backGroundLabel.setBounds(0,0,1120,630);

		

		initAdminPanel();
		//initViewPanel();
		//initAddPanel();
		//initRemovePanel();
		//initUpdatePanel();
		//clickedBar.add(removePanel);
		defaultMethod();

	}













	public void initMyPanel()
	{
		myPanel = new JPanel();
		myPanel.setLayout(null);
		myPanel.setOpaque(false);







		userLabelMP = new JLabel("   User Name");
		userLabelMP.setBounds(150,40,120,40);
		userLabelMP.setFont(f1);
		userLabelMP.setOpaque(true);
		userLabelMP.setBackground(Color.white);
		userLabelMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		userLabelMP.setHorizontalAlignment(SwingConstants.LEFT);
		userLabelMP.setForeground(kasper);
		myPanel.add(userLabelMP);
		
		userTFMP = new JTextField(currentUserName);
		userTFMP.setBounds(270,40,490,40);
		userTFMP.setFont(f2);
		userTFMP.setBackground(Color.white);
		userTFMP.setForeground(Color.gray);
		userTFMP.setBorder(BorderFactory.createEmptyBorder());
		userTFMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		userTFMP.setHorizontalAlignment(SwingConstants.LEFT);
		userTFMP.setEditable(false);
		myPanel.add(userTFMP);

		
		eNameLabelMP = new JLabel("   Employee Name");
		eNameLabelMP.setBounds(150,100,120,40);
		eNameLabelMP.setFont(f1);
		eNameLabelMP.setOpaque(true);
		eNameLabelMP.setBackground(Color.white);
		eNameLabelMP.setForeground(kasper);
		eNameLabelMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		myPanel.add(eNameLabelMP);
		
		eNameTFMP = new JTextField();
		eNameTFMP.setBounds(270,100,490,40);
		eNameTFMP.setFont(f2);
		eNameTFMP.setBackground(Color.white);
		eNameTFMP.setForeground(Color.GRAY);
		eNameTFMP.setBorder(BorderFactory.createEmptyBorder());
		eNameTFMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		eNameTFMP.setHorizontalAlignment(SwingConstants.LEFT);
		myPanel.add(eNameTFMP);
		
		phoneLabelMP = new JLabel("   Phone");
		phoneLabelMP.setBounds(150,160,120,40);
		phoneLabelMP.setFont(f1);
		phoneLabelMP.setOpaque(true);
		phoneLabelMP.setBackground(Color.white);
		phoneLabelMP.setForeground(kasper);
		phoneLabelMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		myPanel.add(phoneLabelMP);
		
		phoneTFMP = new JTextField();
		phoneTFMP.setBounds(270,160,490,40);
		phoneTFMP.setFont(f2);
		//phoneTFUP.setOpaque(false);
		phoneTFMP.setBackground(Color.white);
		phoneTFMP.setForeground(Color.GRAY);
		phoneTFMP.setBorder(BorderFactory.createEmptyBorder());
		phoneTFMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		phoneTFMP.setHorizontalAlignment(SwingConstants.LEFT);
		myPanel.add(phoneTFMP);



		passLabelMP = new JLabel("   Password");
		passLabelMP.setBounds(150,220,120,40);
		passLabelMP.setFont(f1);
		passLabelMP.setOpaque(true);
		passLabelMP.setBackground(Color.white);
		passLabelMP.setForeground(kasper);
		passLabelMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		myPanel.add(passLabelMP);
		
		passPFMP = new JPasswordField();
		passPFMP.setBounds(270,220,490,40);
		passPFMP.setFont(f2);
		//phoneTFUP.setOpaque(false);
		passPFMP.setBackground(Color.white);
		passPFMP.setForeground(Color.GRAY);
		passPFMP.setBorder(BorderFactory.createEmptyBorder());
		passPFMP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		passPFMP.setHorizontalAlignment(SwingConstants.LEFT);
		passPFMP.addFocusListener(this);
		myPanel.add(passPFMP);

		
		clearBtnMP = new JButton("Clear");
		clearBtnMP.setBounds(150, 300, 305, 40);
		clearBtnMP.setFont(f1);
		clearBtnMP.setForeground(Color.gray);
		clearBtnMP.setBackground(Color.white);
		clearBtnMP.setFocusPainted(false);
		clearBtnMP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		clearBtnMP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		clearBtnMP.addActionListener(this);
		myPanel.add(clearBtnMP);
		


		updateBtnMP = new JButton("Update");
		updateBtnMP.setBounds(455, 300, 305, 40);
		updateBtnMP.setFont(f1);
		updateBtnMP.setForeground(kasper);
		updateBtnMP.setBackground(Color.white);
		updateBtnMP.setFocusPainted(false);
		updateBtnMP.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, kasper));
		updateBtnMP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		updateBtnMP.addActionListener(this);
		myPanel.add(updateBtnMP);


		myInfo();
		logPortion();
	}

	public void myInfo()
	{

		String loadId =userTFMP.getText();// userTFMP.getText();
		///tempID=loadId;
		String query = "SELECT `employeeName`, `phoneNumber` FROM `employee` WHERE `userName`='"+loadId+"';";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;
			String eName = null;
			String phnNo = null;
					
			while(rs.next())
			{
                eName = rs.getString("employeeName");
				phnNo = rs.getString("phoneNumber");
				flag=true;
				
				eNameTFMP.setText(eName);
				currentUserLabel.setText(eName);
				currentUserLabel.repaint();
				phoneTFMP.setText(phnNo);
				
				updateBtnMP.setEnabled(true);
				flagUP=1;

				logPortion();
				//delBtn.setEnabled(true);
			}
			if(!flag)
			{
				eNameTFMP.setText("");
				phoneTFMP.setText("");
				updateBtnMP.setEnabled(false);
				

				JOptionPane.showMessageDialog(this,"Invalid ID"); 
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }


	}


	public void logPortion()
	{


		String loadId =userTFMP.getText();
		///tempID=loadId;
		String query = "SELECT `Password` FROM `login` WHERE `userName`='"+loadId+"';";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;
			String pass = null;
					
			while(rs.next())
			{
                pass = rs.getString("Password");
				flag=true;
				
				passPFMP.setText(pass);
				
				updateBtnMP.setEnabled(true);
				flagUP=1;
				//delBtn.setEnabled(true);
			}
			if(!flag)
			{
				eNameTFMP.setText("");
				phoneTFMP.setText("");
				updateBtnMP.setEnabled(false);
				

				JOptionPane.showMessageDialog(this,"Invalid ID"); 
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }

	}









	public void updateMyDB()
	{
		
		String newId;
		String eName;
		String phnNo;
		String pass;

		try
		{
			newId = userTFMP.getText();
			eName = eNameTFMP.getText();
			phnNo = phoneTFMP.getText();
			pass =  passPFMP.getText();

		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(this, "Plase check inserted iformations again");
			return;
		}
		String query = "UPDATE employee SET employeeName='"+eName+"', phoneNumber = '"+phnNo+"' WHERE userName='"+newId+"'";
		//System.out.println(status);
		String query2 = "UPDATE Login SET Password="+pass+" WHERE userName='"+newId+"'";
		
        Connection con=null;//for connection
        Statement st = null;//for query execution
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			st = con.createStatement();//create statement
			st.executeUpdate(query);
			st.executeUpdate(query2);
			st.close();
			con.close();

			currentUserLabel.setText(eName);
			currentUserLabel.repaint();
			JOptionPane.showMessageDialog(this, "Account Updated Successfully");
			
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(this, "Sorry! Something Went Wrong");
		}
	}



























	public void initAdminPanel()
	{
		adminPanel = new JLayeredPane();
		adminPanel.setLayout(null);

		sideBar = new JPanel();
		sideBar.setLayout(null);
		sideBar.setBounds(0,70,150,580);
		sideBar.setOpaque(false);
		

		clickedBar = new JPanel();
		clickedBar.setLayout(new GridLayout(1,1));
		clickedBar.setBounds(150,70,970,580);
		clickedBar.setOpaque(false);
		//clickedBar.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));


		backButton.setBounds(500,200,100,100);
		backButton.addActionListener(this);

		adminHeader = new JLabel(" Reception");
		adminHeader.setBounds(0,0,1120,70);
		adminHeader.setFont(f4);
		adminHeader.setOpaque(true);
		adminHeader.setForeground(kasper);
		adminHeader.setBackground(Color.white);
		//adminHeader.setForeground(Color.white);
		//adminHeader.setFocusPainted(false);
		adminHeader.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, kasper));
		adminHeader.setVerticalAlignment(JLabel.CENTER);
		adminHeader.setHorizontalAlignment(JLabel.LEFT);

		currentUserLabel = new JLabel(currentUserName);
		currentUserLabel.setBounds(300,15,682,40);
		currentUserLabel.setFont(f2);
		//currentUserLabel.setOpaque(true);
		currentUserLabel.setForeground(Color.gray);
		currentUserLabel.setBackground(Color.gray);
		currentUserLabel.setVerticalAlignment(JLabel.CENTER);
		currentUserLabel.setHorizontalAlignment(JLabel.RIGHT);


		logoutButton = new JButton("Logout");
		logoutButton.setBounds(997,15,100,40);
		logoutButton.setFont(f2);
		logoutButton.setForeground(bloodLake);
		logoutButton.setBackground(Color.white);
		logoutButton.setFocusPainted(false);
		logoutButton.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.gray));
		logoutButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		logoutButton.addActionListener(this);


		myProfileBtn = new JButton("My Profile");
		myProfileBtn.setBounds(0,40,150,40);
		myProfileBtn.setFont(f1);
		myProfileBtn.setForeground(Color.gray);
		myProfileBtn.setBackground(Color.white);
		myProfileBtn.setForeground(Color.gray);
		myProfileBtn.setFocusPainted(false);
		myProfileBtn.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		myProfileBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		myProfileBtn.addActionListener(this);



		viewPatients = new JButton("View Patients");
		viewPatients.setBounds(0,40+40,150,40);
		viewPatients.setFont(f1);
		viewPatients.setForeground(Color.gray);
		viewPatients.setBackground(Color.white);
		viewPatients.setForeground(Color.gray);
		viewPatients.setFocusPainted(false);
		viewPatients.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		viewPatients.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		viewPatients.addActionListener(this);

		addPatients = new JButton("Admit Patients");
		addPatients.setBounds(0,80+40,150,40);
		addPatients.setFont(f1);
		addPatients.setForeground(Color.gray);
		addPatients.setBackground(Color.white);
		addPatients.setForeground(Color.gray);
		addPatients.setFocusPainted(false);
		addPatients.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		addPatients.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		addPatients.addActionListener(this);

		updatePatients = new JButton("Update Patients");
		updatePatients.setBounds(0,120+40,150,40);
		updatePatients.setFont(f1);
		updatePatients.setForeground(Color.gray);
		updatePatients.setBackground(Color.white);
		updatePatients.setForeground(Color.gray);
		updatePatients.setFocusPainted(false);
		updatePatients.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		updatePatients.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		updatePatients.addActionListener(this);

		removePatients = new JButton("Release Patients");
		removePatients.setBounds(0,160+40,150,40);
		removePatients.setFont(f1);
		removePatients.setForeground(Color.gray);
		removePatients.setBackground(Color.white);
		removePatients.setForeground(Color.gray);
		removePatients.setFocusPainted(false);
		removePatients.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		removePatients.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		removePatients.addActionListener(this);


		profileButtonsArray[0]=myProfileBtn;
		profileButtonsArray[1]=viewPatients;
		profileButtonsArray[2]=addPatients;
		profileButtonsArray[3]=updatePatients;
		profileButtonsArray[4]=removePatients;

		

		sideBar.add(myProfileBtn);
		sideBar.add(viewPatients);
		sideBar.add(addPatients);
		sideBar.add(updatePatients);
		sideBar.add(removePatients);

		adminPanel.add(sideBar);
		adminPanel.add(backGroundLabel, Integer.valueOf(1));

    	adminPanel.add(logoutButton, Integer.valueOf(4));
    	adminPanel.add(currentUserLabel,Integer.valueOf(3));
    	adminPanel.add(adminHeader, Integer.valueOf(2));
    	adminPanel.add(sideBar, Integer.valueOf(5));
    	adminPanel.add(clickedBar, Integer.valueOf(6));


		//adminPanel.add(backGroundLabel);
		add(adminPanel);
	}

	public void initViewPanel()
	{

		viewPanel = new JPanel();
		viewPanel.setLayout(null);
		viewPanel.setOpaque(false);



		String query = "SELECT *  FROM `Patient`;"; 
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try 
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");


			
			int c=0;
			String[][] data = new String[100][7];
			String column[] = {"Patient ID", "Patient Name", "Age","Blood Group","Gender","Phone No.", "Release Permit"};

			int j,z=0;
			for(int i = 0; rs.next(); i++){
				
				
				j = 0;
				data[i][j++] = rs.getString("patientID");
				data[i][j++] = rs.getString("patientName");
				data[i][j++] = rs.getString("patientAge");
				data[i][j++] = rs.getString("bloodGroup");
				data[i][j++] = rs.getString("Gender");
				data[i][j++] = rs.getString("phoneNumber");
				data[i][j++] = rs.getString("releasePermit");

				c++;
				z=z+40;
			}

			String [][] newData = new String[c][7];
			for(int i=0; i<c; i++)
			{
				for(int j2=0; j2<7; j2++)
				{
					newData[i][j2]=data[i][j2];
				}
			    
			}


			  

			tTable = new JTable(newData, column){
			    DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

			    { 
			        renderRight.setHorizontalAlignment(SwingConstants.CENTER);
			    }

			    @Override
			    public TableCellRenderer getCellRenderer (int arg0, int arg1) {
			        return renderRight;
			    };
			};
			
			tTable.setFont(f1);
			tTable.setOpaque(false);
			tTable.setForeground(Color.GRAY);
			tTable.setBackground(Color.white);
			tTable.setRowHeight(tTable.getRowHeight()+20);
    		tScroll = new JScrollPane(tTable);
			tScroll.setBounds(50,40,850,450);  


		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }

		
		viewPanel.add(tScroll);
	}



	public void initUpdatePanel()
	{
		updatePanel = new JPanel();
		updatePanel.setLayout(null);
		updatePanel.setOpaque(false);







		patientIDLabelUP = new JLabel("  Patient ID");
		patientIDLabelUP.setBounds(150,40,120,40);
		patientIDLabelUP.setFont(f1);
		patientIDLabelUP.setOpaque(true);
		patientIDLabelUP.setBackground(Color.white);
		patientIDLabelUP.setForeground(kasper);


		updatePanel.add(patientIDLabelUP);
		
		patientIDTFUP = new JTextField();
		patientIDTFUP.setBounds(270,40,340,40);
		patientIDTFUP.setFont(f2);
		patientIDTFUP.setBackground(Color.white);
		patientIDTFUP.setForeground(Color.GRAY);
		patientIDTFUP.setBorder(BorderFactory.createEmptyBorder());
		patientIDTFUP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		patientIDTFUP.setHorizontalAlignment(SwingConstants.LEFT);
		updatePanel.add(patientIDTFUP);


		loadPatientUP = new JButton("Load Info");
		loadPatientUP.setBounds(610,40,150,40);
		loadPatientUP.setFont(f1);
		loadPatientUP.setBackground(Color.white);
		loadPatientUP.setForeground(Color.gray);
		loadPatientUP.setFocusPainted(false);
		loadPatientUP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		loadPatientUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		loadPatientUP.addActionListener(this);
		updatePanel.add(loadPatientUP);


		
		
		
		pNameLabelUP = new JLabel("   Patient Name");
		pNameLabelUP.setBounds(150,100,120,40);
		pNameLabelUP.setFont(f1);
		pNameLabelUP.setOpaque(true);
		pNameLabelUP.setBackground(Color.white);
		pNameLabelUP.setForeground(kasper);
		updatePanel.add(pNameLabelUP);
		
		pNameTFUP = new JTextField();
		pNameTFUP.setBounds(270,100,490,40);
		pNameTFUP.setFont(f2);
		pNameTFUP.setBackground(Color.white);
		pNameTFUP.setForeground(Color.GRAY);
		pNameTFUP.setBorder(BorderFactory.createEmptyBorder());
		pNameTFUP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		pNameTFUP.setHorizontalAlignment(SwingConstants.LEFT);
		updatePanel.add(pNameTFUP);


		pAgeLabelUP = new JLabel("   Patient Age");
		pAgeLabelUP.setBounds(150,160,120,40);
		pAgeLabelUP.setFont(f1);
		pAgeLabelUP.setOpaque(true);
		pAgeLabelUP.setBackground(Color.white);
		pAgeLabelUP.setForeground(kasper);
		updatePanel.add(pAgeLabelUP);
		
		pAgeTFUP = new JTextField();
		pAgeTFUP.setBounds(270,160,490,40);
		pAgeTFUP.setFont(f2);
		pAgeTFUP.setBackground(Color.white);
		pAgeTFUP.setForeground(Color.GRAY);
		pAgeTFUP.setBorder(BorderFactory.createEmptyBorder());
		pAgeTFUP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		pAgeTFUP.setHorizontalAlignment(SwingConstants.LEFT);
		updatePanel.add(pAgeTFUP);

		
		phoneLabelUP = new JLabel("   Phone");
		phoneLabelUP.setBounds(150,340,120,40);
		phoneLabelUP.setFont(f1);
		phoneLabelUP.setOpaque(true);
		phoneLabelUP.setBackground(Color.white);
		phoneLabelUP.setForeground(kasper);
		updatePanel.add(phoneLabelUP);
		
		phoneTFUP = new JTextField();
		//phoneTF1.setEnabled(false);
		phoneTFUP.setBounds(270,340,490,40);
		phoneTFUP.setFont(f2);
		phoneTFUP.setBackground(Color.white);
		phoneTFUP.setForeground(Color.GRAY);
		phoneTFUP.setBorder(BorderFactory.createEmptyBorder());
		phoneTFUP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		phoneTFUP.setHorizontalAlignment(SwingConstants.LEFT);
		updatePanel.add(phoneTFUP);
		
		
		bloodLabelUP = new JLabel("   Blood Group");
		bloodLabelUP.setBounds(150,220,120,40);
		bloodLabelUP.setFont(f1);
		bloodLabelUP.setOpaque(true);
		bloodLabelUP.setBackground(Color.white);
		bloodLabelUP.setForeground(kasper);
		updatePanel.add(bloodLabelUP);
		
		String []items = {"Unknown","A(+)","A(-)","B(+)","B(-)","O(+)","O(-)"};
		bloodComboUP = new JComboBox(items);
		bloodComboUP.setBounds(270,220,490,40);
		bloodComboUP.setFont(f1);
		bloodComboUP.setOpaque(true);
		bloodComboUP.setBackground(Color.white);
		bloodComboUP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.white));
		((JLabel)bloodComboUP.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
		bloodComboUP.setForeground(kasper);
		updatePanel.add(bloodComboUP);


		genderLabelUP = new JLabel("   Gender");
		genderLabelUP.setBounds(150,280,120,40);
		genderLabelUP.setFont(f1);
		genderLabelUP.setOpaque(true);
		genderLabelUP.setBackground(Color.white);
		genderLabelUP.setForeground(kasper);
		updatePanel.add(genderLabelUP);
		
		String []items2 = {"Male","Female"};
		genderComboUP = new JComboBox(items2);
		genderComboUP.setBounds(270,280,490,40);
		genderComboUP.setFont(f1);
		genderComboUP.setOpaque(true);
		genderComboUP.setBackground(Color.white);
		genderComboUP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.white));
		((JLabel)genderComboUP.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
		genderComboUP.setForeground(kasper);
		updatePanel.add(genderComboUP);
		/*
		salaryLabelAP = new JLabel("   Salary:");
		salaryLabelAP.setBounds(150,340,120,40);
		salaryLabelAP.setFont(f1);
		salaryLabelAP.setOpaque(true);
		salaryLabelAP.setBackground(Color.white);
		salaryLabelAP.setForeground(kasper);
		addPanel.add(salaryLabelAP);
		
		salaryTFAP = new JTextField();
		salaryTFAP.setBounds(270,340,490,40);
		salaryTFAP.setFont(f2);
		salaryTFAP.setBackground(Color.white);
		salaryTFAP.setForeground(Color.GRAY);
		salaryTFAP.setBorder(BorderFactory.createEmptyBorder());
		salaryTFAP.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, kasper));
		salaryTFAP.setHorizontalAlignment(SwingConstants.LEFT);
		addPanel.add(salaryTFAP);
		*/
		
		
		clearBtnUP = new JButton("Clear");
		clearBtnUP.setBounds(149, 400, 305, 40);
		clearBtnUP.setFont(f1);
		clearBtnUP.setForeground(Color.gray);
		clearBtnUP.setBackground(Color.white);
		clearBtnUP.setFocusPainted(false);
		clearBtnUP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		clearBtnUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		clearBtnUP.addActionListener(this);
		updatePanel.add(clearBtnUP);



		updateBtnUP = new JButton("Update");
		updateBtnUP.setBounds(455, 400, 306, 40);
		updateBtnUP.setFont(f1);
		updateBtnUP.setBackground(Color.white);
		updateBtnUP.setForeground(kasper);
		updateBtnUP.setFocusPainted(false);
		updateBtnUP.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, kasper));
		updateBtnUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		updateBtnUP.addActionListener(this);
		updateBtnUP.setEnabled(false);
		updatePanel.add(updateBtnUP);
		
		

		
	}

	public void initAddPanel()
	{
		addPanel = new JPanel();
		addPanel.setLayout(null);
		addPanel.setOpaque(false);


		patientIDLabelAP = new JLabel("   Patient ID");
		patientIDLabelAP.setBounds(150,40,120,40);
		patientIDLabelAP.setFont(f1);
		patientIDLabelAP.setOpaque(true);
		patientIDLabelAP.setBackground(Color.white);
		patientIDLabelAP.setForeground(kasper);


		addPanel.add(patientIDLabelAP);
		
		patientIDTFAP = new JTextField();
		patientIDTFAP.setBounds(270,40,490,40);
		patientIDTFAP.setFont(f2);
		patientIDTFAP.setBackground(Color.white);
		patientIDTFAP.setForeground(Color.GRAY);
		patientIDTFAP.setBorder(BorderFactory.createEmptyBorder());
		patientIDTFAP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		patientIDTFAP.setHorizontalAlignment(SwingConstants.LEFT);
		addPanel.add(patientIDTFAP);

		/*
		passLabelAP = new JLabel("   Password:");
		passLabelAP.setBounds(150,100,120,40);
		passLabelAP.setFont(f1);
		passLabelAP.setOpaque(true);
		passLabelAP.setBackground(Color.white);
		passLabelAP.setForeground(kasper);
		addPanel.add(passLabelAP);
		
		passTFAP = new JTextField();
		passTFAP.setBounds(270,100,390,40);
		passTFAP.setFont(f2);
		passTFAP.setBackground(Color.white);
		passTFAP.setForeground(Color.GRAY);
		passTFAP.setBorder(BorderFactory.createEmptyBorder());
		passTFAP.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, kasper));
		passTFAP.setHorizontalAlignment(SwingConstants.LEFT);
		passTFAP.setEnabled(false);
		addPanel.add(passTFAP);
		
		autoPassBtnAP = new JButton("Generate");
		autoPassBtnAP.setBounds(660, 100, 100, 40);
		autoPassBtnAP.setFont(f1);
		autoPassBtnAP.setBackground(kasper);
		autoPassBtnAP.setForeground(Color.white);
		autoPassBtnAP.setFocusPainted(false);
		autoPassBtnAP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		autoPassBtnAP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		autoPassBtnAP.addActionListener(this);
		addPanel.add(autoPassBtnAP);

		*/
		
		pNameLabelAP = new JLabel("   Patient Name");
		pNameLabelAP.setBounds(150,100,120,40);
		pNameLabelAP.setFont(f1);
		pNameLabelAP.setOpaque(true);
		pNameLabelAP.setBackground(Color.white);
		pNameLabelAP.setForeground(kasper);
		addPanel.add(pNameLabelAP);
		
		pNameTFAP = new JTextField();
		pNameTFAP.setBounds(270,100,490,40);
		pNameTFAP.setFont(f2);
		pNameTFAP.setBackground(Color.white);
		pNameTFAP.setForeground(Color.GRAY);
		pNameTFAP.setBorder(BorderFactory.createEmptyBorder());
		pNameTFAP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		pNameTFAP.setHorizontalAlignment(SwingConstants.LEFT);
		addPanel.add(pNameTFAP);


		pAgeLabelAP = new JLabel("   Patient Age");
		pAgeLabelAP.setBounds(150,160,120,40);
		pAgeLabelAP.setFont(f1);
		pAgeLabelAP.setOpaque(true);
		pAgeLabelAP.setBackground(Color.white);
		pAgeLabelAP.setForeground(kasper);
		addPanel.add(pAgeLabelAP);
		
		pAgeTFAP = new JTextField();
		pAgeTFAP.setBounds(270,160,490,40);
		pAgeTFAP.setFont(f2);
		pAgeTFAP.setBackground(Color.white);
		pAgeTFAP.setForeground(Color.GRAY);
		pAgeTFAP.setBorder(BorderFactory.createEmptyBorder());
		pAgeTFAP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		pAgeTFAP.setHorizontalAlignment(SwingConstants.LEFT);
		addPanel.add(pAgeTFAP);

		
		phoneLabelAP = new JLabel("   Phone");
		phoneLabelAP.setBounds(150,340,120,40);
		phoneLabelAP.setFont(f1);
		phoneLabelAP.setOpaque(true);
		phoneLabelAP.setBackground(Color.white);
		phoneLabelAP.setForeground(kasper);
		addPanel.add(phoneLabelAP);
		
		phoneTFAP = new JTextField();
		//phoneTF1.setEnabled(false);
		phoneTFAP.setBounds(270,340,490,40);
		phoneTFAP.setFont(f2);
		phoneTFAP.setBackground(Color.white);
		phoneTFAP.setForeground(Color.GRAY);
		phoneTFAP.setBorder(BorderFactory.createEmptyBorder());
		phoneTFAP.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, kasper));
		phoneTFAP.setHorizontalAlignment(SwingConstants.LEFT);
		addPanel.add(phoneTFAP);
		
		
		bloodLabelAP = new JLabel("   Blood Group");
		bloodLabelAP.setBounds(150,220,120,40);
		bloodLabelAP.setFont(f1);
		bloodLabelAP.setOpaque(true);
		bloodLabelAP.setBackground(Color.white);
		bloodLabelAP.setForeground(kasper);
		addPanel.add(bloodLabelAP);
		
		String []items = {"Unknown","A(+)","A(-)","B(+)","B(-)","O(+)","O(-)"};
		bloodComboAP = new JComboBox(items);
		bloodComboAP.setBounds(270,220,490,40);
		bloodComboAP.setFont(f1);
		bloodComboAP.setOpaque(true);
		bloodComboAP.setBackground(Color.white);
		bloodComboAP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.white));
		((JLabel)bloodComboAP.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
		bloodComboAP.setForeground(kasper);
		addPanel.add(bloodComboAP);


		genderLabelAP = new JLabel("   Gender");
		genderLabelAP.setBounds(150,280,120,40);
		genderLabelAP.setFont(f1);
		genderLabelAP.setOpaque(true);
		genderLabelAP.setBackground(Color.white);
		genderLabelAP.setForeground(kasper);
		addPanel.add(genderLabelAP);
		
		String []items2 = {"Male","Female"};
		genderComboAP = new JComboBox(items2);
		genderComboAP.setBounds(270,280,490,40);
		genderComboAP.setFont(f1);
		genderComboAP.setOpaque(true);
		genderComboAP.setBackground(Color.white);
		genderComboAP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.white));
		((JLabel)genderComboAP.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
		genderComboAP.setForeground(kasper);
		addPanel.add(genderComboAP);
		/*
		salaryLabelAP = new JLabel("   Salary:");
		salaryLabelAP.setBounds(150,340,120,40);
		salaryLabelAP.setFont(f1);
		salaryLabelAP.setOpaque(true);
		salaryLabelAP.setBackground(Color.white);
		salaryLabelAP.setForeground(kasper);
		addPanel.add(salaryLabelAP);
		
		salaryTFAP = new JTextField();
		salaryTFAP.setBounds(270,340,490,40);
		salaryTFAP.setFont(f2);
		salaryTFAP.setBackground(Color.white);
		salaryTFAP.setForeground(Color.GRAY);
		salaryTFAP.setBorder(BorderFactory.createEmptyBorder());
		salaryTFAP.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, kasper));
		salaryTFAP.setHorizontalAlignment(SwingConstants.LEFT);
		addPanel.add(salaryTFAP);
		*/
		
		
		clearBtnAP = new JButton("Clear");
		clearBtnAP.setBounds(149, 400, 305, 40);
		clearBtnAP.setFont(f1);
		clearBtnAP.setForeground(Color.gray);
		clearBtnAP.setBackground(Color.white);
		clearBtnAP.setFocusPainted(false);
		clearBtnAP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		clearBtnAP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		clearBtnAP.addActionListener(this);
		addPanel.add(clearBtnAP);



		addBtnAP = new JButton("Add");
		addBtnAP.setBounds(455, 400, 306, 40);
		addBtnAP.setFont(f1);
		addBtnAP.setForeground(kasper);
		addBtnAP.setBackground(Color.white);
		addBtnAP.setFocusPainted(false);
		addBtnAP.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, kasper));
		addBtnAP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		addBtnAP.addActionListener(this);
		addPanel.add(addBtnAP);













	}

	public void initRemovePanel()
	{
		removePanel = new JPanel();
		removePanel.setLayout(null);
		removePanel.setOpaque(false);
		//removePanel.setBackground(Color.white);

		patientIDLabelRP = new JLabel("   User Name");
		patientIDLabelRP.setBounds(50,40,110,40);
		patientIDLabelRP.setFont(f1);
		patientIDLabelRP.setOpaque(true);
		patientIDLabelRP.setBackground(Color.white);
		patientIDLabelRP.setForeground(kasper);

		patientIDTFRP = new JTextField();
		patientIDTFRP.setBounds(160,40,585,40);
		patientIDTFRP.setFont(f2);
		//patientIDTFRP.setOpaque(true);
		patientIDTFRP.setBackground(Color.white);
		patientIDTFRP.setForeground(Color.GRAY);
		patientIDTFRP.setBorder(BorderFactory.createEmptyBorder());
		patientIDTFRP.setHorizontalAlignment(SwingConstants.CENTER);

		findPatientRP = new JButton("Find");
		findPatientRP.setBounds(745,40,150,40);
		findPatientRP.setFont(f1);
		findPatientRP.setForeground(Color.gray);
		findPatientRP.setBackground(Color.white);
		findPatientRP.setFocusPainted(false);
		findPatientRP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
		findPatientRP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		findPatientRP.addActionListener(this);

		removePatientRP = new JButton("Release Patient");
		removePatientRP.setBounds(50,250,850,40);
		removePatientRP.setFont(f2);
		removePatientRP.setBackground(bloodLake);
		removePatientRP.setForeground(Color.white);
		removePatientRP.setFocusPainted(false);
		removePatientRP.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, bloodLake));
		removePatientRP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		removePatientRP.addActionListener(this);




		removePanel.add(findPatientRP);
		removePanel.add(patientIDTFRP);
		removePanel.add(patientIDLabelRP);

	}

	public void actionPerformed(ActionEvent ae)
	{
		if(tempButton==(JButton)ae.getSource())
		{
			return;
		}
		if(tempButton==null)
		{
			tempButton=(JButton)ae.getSource();
		}
		

		if(		(JButton)ae.getSource()==logoutButton		)
		{
			lf.setVisible(true);
			this.setVisible(false);
		}
		if(		(JButton)ae.getSource()==myProfileBtn		)
		{
			defaultMethod();
		}
		if(		(JButton)ae.getSource()==viewPatients		)
		{
			
			for(int i=0; i<profileButtonsArray.length; i++)
			{
				
				if(profileButtonsArray[i]==viewPatients)
				{
					profileButtonsArray[i].setBackground(kasper);
					profileButtonsArray[i].setForeground(Color.white);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.white));
				}
				else
				{
					profileButtonsArray[i].setBackground(Color.white);
					profileButtonsArray[i].setForeground(kasper);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
				}
			}
			
			
			clickedBar.removeAll();
			initViewPanel();
			clickedBar.add(viewPanel);			
			clickedBar.repaint();
			tempButton=(JButton)ae.getSource();
		}
		
		


		if(		(JButton)ae.getSource()==addPatients		)
		{
			
			for(int i=0; i<profileButtonsArray.length; i++)
			{
				
				if(profileButtonsArray[i]==addPatients)
				{
					profileButtonsArray[i].setBackground(kasper);
					profileButtonsArray[i].setForeground(Color.white);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.white));
				}
				else
				{
					profileButtonsArray[i].setBackground(Color.white);
					profileButtonsArray[i].setForeground(kasper);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
				}
			}
			
			initAddPanel();
			clickedBar.removeAll();
			clickedBar.add(addPanel);
			
			clickedBar.repaint();
			tempButton=(JButton)ae.getSource();

			
			//clickedBar.repaint();
		}
		if(		(JButton)ae.getSource()==updatePatients		)
		{
			
			for(int i=0; i<profileButtonsArray.length; i++)
			{
				
				if(profileButtonsArray[i]==updatePatients)
				{
					profileButtonsArray[i].setBackground(kasper);
					profileButtonsArray[i].setForeground(Color.white);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(0 ,0, 0, 0, Color.white));
				}
				else
				{
					profileButtonsArray[i].setBackground(Color.white);
					profileButtonsArray[i].setForeground(kasper);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
				}
			}
			
			initUpdatePanel();
			clickedBar.removeAll();
			clickedBar.add(updatePanel);
			
			clickedBar.repaint();
			tempButton=(JButton)ae.getSource();
		}
		if(		(JButton)ae.getSource()==removePatients		)
		{
			
			for(int i=0; i<profileButtonsArray.length; i++)
			{
				
				if(profileButtonsArray[i]==removePatients)
				{
					profileButtonsArray[i].setBackground(kasper);
					profileButtonsArray[i].setForeground(Color.white);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.white));
				}
				else
				{
					profileButtonsArray[i].setBackground(Color.white);
					profileButtonsArray[i].setForeground(kasper);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
				}
			}
			
			initRemovePanel();

			clickedBar.removeAll();
			clickedBar.add(removePanel);
			clickedBar.repaint();
			tempButton=(JButton)ae.getSource();
		}

		

		if((JButton)ae.getSource()==findPatientRP)
		{
			if(flagRP==1)
			{
				removePanel.remove(tableScrollPane);
				removePanel.remove(removePatientRP);
				removePanel.repaint();
				flagRP=0;
			}
			findInfoDB();
		}
		if((JButton)ae.getSource()==removePatientRP)
		{
			
			
			removeInfoDB();
			removePanel.remove(tableScrollPane);
			removePanel.remove(removePatientRP);
			removePanel.repaint();
			
		}
		if((JButton)ae.getSource()==addBtnAP)
		{
			
			addInfoDB();
		}

		

		if((JButton)ae.getSource()==loadPatientUP)
		{
			if(flagUP==1)
			{
				updatePanel.repaint();
				flagUP=0;
			}
			loadFromDB();
		}
		if((JButton)ae.getSource()==updateBtnUP)
		{
			updateInDB();
		}

		if((JButton)ae.getSource()==clearBtnAP)
		{
			clearAP();
		}
		if((JButton)ae.getSource()==clearBtnUP)
		{
			clearUP();
		}
		if((JButton)ae.getSource()==updateBtnMP)
		{
			updateMyDB();
		}

		if((JButton)ae.getSource()==clearBtnMP)
		{
			clearMP();
		}






		
	}


	public void focusGained(FocusEvent fe)
	{
		passPFMP.setEchoChar((char) 0);
	}
	public void focusLost(FocusEvent fe)
	{
		passPFMP.setEchoChar('*');
	}


	public void clearMP()
	{
		myInfo();
		logPortion();
		//defaultMethod();
	}




	public void clearAP()
	{
		patientIDTFAP.setText("");
		pNameTFAP.setText("");
		pAgeTFAP.setText("");
		phoneTFAP.setText("");
	}
	public void clearUP()
	{
		patientIDTFUP.setText("");
		pNameTFUP.setText("");
		pAgeTFUP.setText("");
		bloodComboUP.setSelectedItem(null);
		genderComboUP.setSelectedItem(null);
		phoneTFUP.setText("");
		updateBtnUP.setEnabled(false);
	}


	public void loadFromDB()
	{
		String loadId = patientIDTFUP.getText();
		tempID=loadId;
		String query = "SELECT `patientName`, `patientAge`, `bloodGroup`, `Gender`, `phoneNumber` FROM `Patient` WHERE `patientID`='"+loadId+"';";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;
			String pName = null;
			String pAge = null;
			String blood = null;
			String gender = null;
			String phnNo = null;
			while(rs.next())
			{
                pName = rs.getString("patientName");
				pAge = rs.getString("patientAge");
				blood = rs.getString("bloodGroup");
				gender = rs.getString("Gender");
				phnNo = rs.getString("phoneNumber");
				flag=true;
				
				pNameTFUP.setText(pName);
				pAgeTFUP.setText(pAge);
				bloodComboUP.setSelectedItem(blood);
				genderComboUP.setSelectedItem(gender);
				phoneTFUP.setText(phnNo);
				updateBtnUP.setEnabled(true);
				flagUP=1;
			}
			if(!flag)
			{
				pNameTFUP.setText("");
				pAgeTFUP.setText("");
				bloodComboUP.setSelectedItem(null);
				genderComboUP.setSelectedItem(null);
				phoneTFUP.setText("");
				JOptionPane.showMessageDialog(this,"Invalid ID"); 
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }
	}
	
	public void updateInDB()
	{
		
		System.out.println("LOOLLLOOO");
		String newId = null;
		String pName = null;
		String pAge = null;
		String blood = null;
		String gender = null;
		String phnNo = null;

		try
		{
			newId = patientIDTFUP.getText();
			pName = pNameTFUP.getText();
			pAge = pAgeTFUP.getText();
			blood=bloodComboUP.getSelectedItem().toString();
			gender = genderComboUP.getSelectedItem().toString();
			phnNo = phoneTFUP.getText();
			

		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(this, "Plase check inserted iformations again");
			return;
		}
		String query = "UPDATE Patient SET patientName='"+pName+"', patientAge = '"+pAge+"', bloodGroup = '"+blood+"', Gender = '"+gender+"' , phoneNumber = '"+phnNo+"' WHERE patientID='"+newId+"'";
		
        Connection con=null;//for connection
        Statement st = null;//for query execution
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			st = con.createStatement();//create statement
			st.executeUpdate(query);
			st.close();
			con.close();

			JOptionPane.showMessageDialog(this, "Account Updated Successfully");
			tempID=newId;
			
			updateBtnUP.setEnabled(false);
			patientIDTFUP.setText("");
			pNameTFUP.setText("");
			pAgeTFUP.setText("");
			bloodComboUP.setSelectedItem(null);
			genderComboUP.setSelectedItem(null);
			phoneTFUP.setText("");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(this, "Sorry! Something Went Wrong");
		}
	}

	


	

	public void addInfoDB()
	{
		
		String newId;
		String pName;
		String pAge;
		String phnNo;
		String gender;
		String blood;
		String rPermit ="no";

		try
		{
			newId = patientIDTFAP.getText();
			pName = pNameTFAP.getText();
			pAge = pAgeTFAP.getText();
			phnNo = phoneTFAP.getText();
			gender = genderComboAP.getSelectedItem().toString();
			blood = bloodComboAP.getSelectedItem().toString();
			
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(this, "Plase check inserted iformations again");
			return;
		}

		
		
		String query1 = "INSERT INTO Patient VALUES ('"+newId+"','"+pName+"','"+pAge+"','"+blood+"','"+gender+"','"+phnNo+"','"+rPermit+"');";
		String query2 = "INSERT INTO treatment VALUES ('"+newId+"','No Instruction Yet');";
		System.out.println(query1);
		System.out.println(query2);
        
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03", "root", "");
			Statement stm = con.createStatement();
			stm.execute(query1);
			stm.execute(query2);
			stm.close();
			con.close();
			JOptionPane.showMessageDialog(this, "Patient Admitted Successfully");

			

			patientIDTFAP.setText("");
			pNameTFAP.setText("");
			pAgeTFAP.setText("");
			phoneTFAP.setText("");
			
		}
        catch(Exception ex)
		{
			
			System.out.println(ex.getMessage());
			JOptionPane.showMessageDialog(this, "Sorry! Something Went Wrong");
        }
    }


	public void findInfoDB()
	{
		String loadId = patientIDTFRP.getText();
		String query = "SELECT `patientID`, `patientName`, `patientName`, `patientAge`, `bloodGroup`, `Gender`, `phoneNumber`,`releasePermit`  FROM `Patient` WHERE `patientID`='"+loadId+"';";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;

			String pID = null;
			String pName=null;
			String pAge=null;
			String phnNo=null;
			String gender=null;
			String blood=null;
			String rPermit=null;

			while(rs.next())
			{

				pID = rs.getString("patientID");
				pName = rs.getString("patientName");
				pAge = rs.getString("patientAge");
				blood = rs.getString("bloodGroup");
				gender = rs.getString("Gender");
				phnNo = rs.getString("phoneNumber");
				rPermit = rs.getString("releasePermit");

				flag=true;



				String [][]row = {{pID, pName,pAge, blood , gender, phnNo, rPermit}};
				String col[] = {"Patient ID", "Patient Name", "Age","Blood Group","Gender","Phone No.", "Release Permit"};
				myTable = new JTable(row,col){
				    DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

				    { // initializer block
				        renderRight.setHorizontalAlignment(SwingConstants.CENTER);
				    }

				    @Override
				    public TableCellRenderer getCellRenderer (int arg0, int arg1) {
				        return renderRight;
				    };
				};
				tableScrollPane = new JScrollPane(myTable);
				tableScrollPane.setBounds(50,150,600+100+100+48,68);
				myTable.setFont(f1);
				myTable.setOpaque(false);
				myTable.setForeground(Color.gray);
				myTable.setBackground(Color.white);
				myTable.setRowHeight(myTable.getRowHeight()+30);
				//myTable.setHorizontalAlignment(SwingConstants.CENTER);

				//DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
				//rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
				//myTable.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
				if(rPermit.equals("yes"))
				{

					removePanel.add(removePatientRP);
					removePatientRP.setEnabled(true);

				}
				else
				{

					removePanel.add(removePatientRP);
					removePatientRP.setEnabled(false);

				}
				removePanel.add(removePatientRP);
				removePanel.add(tableScrollPane);
				removePanel.repaint();
				flagRP=1;


				



				
				/*eNameTF.setText(eName);
				phoneTF.setText(phnNo);
				roleTF.setText(role);
				salaryTF.setText(""+salary);
				userTF.setEnabled(false);
				updateBtn.setEnabled(true);
				delBtn.setEnabled(true);
				*/
			}
			if(!flag)
			{
				JOptionPane.showMessageDialog(this,"Invalid ID"); 
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }
	}


	public void removeInfoDB()
	{
		String newId = patientIDTFRP.getText();
		String query1 = "DELETE from Patient WHERE patientID='"+newId+"';";

		String query2 = "DELETE from treatment WHERE patientId='"+newId+"';";
		System.out.println(query1);
		//System.out.println(query2);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a03", "root", "");
			Statement stm = con.createStatement();
			stm.execute(query1);
			stm.execute(query2);
			stm.close();
			con.close();
			JOptionPane.showMessageDialog(this, "Patient Realesd");
			
		}
        catch(Exception ex)
		{
			JOptionPane.showMessageDialog(this, "Something went wrong");
        }
	}





	public void defaultMethod()
	{
		for(int i=0; i<profileButtonsArray.length; i++)
			{
				
				if(profileButtonsArray[i]==myProfileBtn)
				{
					profileButtonsArray[i].setBackground(kasper);
					profileButtonsArray[i].setForeground(Color.white);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.white));
				}
				else
				{
					profileButtonsArray[i].setBackground(Color.white);
					profileButtonsArray[i].setForeground(kasper);
					profileButtonsArray[i].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, kasper));
				}
			}
			

			initMyPanel();
			clickedBar.removeAll();
			clickedBar.add(myPanel);
			clickedBar.repaint();
			tempButton=myProfileBtn;
	}

















	public static void main(String args[])
	{
		RecepFrame rf = new RecepFrame(new LoginFrame(),"Mahira");
		rf.setVisible(true);
	}

}
