-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2018 at 03:41 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `a03`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `userName` varchar(15) NOT NULL,
  `employeename` varchar(15) NOT NULL,
  `phonenumber` varchar(15) NOT NULL,
  `role` varchar(15) NOT NULL,
  `salary` double(6,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`userName`, `employeename`, `phonenumber`, `role`, `salary`) VALUES
('Anik', 'Dr. Anik Sikder', '01900000000', 'Doctor', 1000.00),
('doc01', 'Dr. Ahmed', '0170000', 'Doctor', 2000.00),
('Mahira', 'Mahira Tabassum', '01700000000', 'Receptionist', 5000.00),
('Nafiz', 'Nafiz F Siam', '01521314102', 'Manager', 500.00),
('Siam', 'Siam Nafiz Fuad', '01500000000', 'Receptionist', 1000.00),
('sis02', 'Sister Two', '0180000000', 'Nurse', 1000.00);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `UserName` varchar(15) NOT NULL,
  `Password` varchar(15) NOT NULL,
  `Status` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`UserName`, `Password`, `Status`) VALUES
('Anik', '123456', 2),
('doc01', '1234', 2),
('Mahira', '1234', 1),
('Nafiz', '1234', 0),
('Siam', '1234', 1),
('sis02', '519720', 3);

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `patientID` varchar(30) NOT NULL,
  `patientName` varchar(30) NOT NULL,
  `patientAge` varchar(8) NOT NULL,
  `bloodGroup` varchar(15) NOT NULL,
  `Gender` varchar(15) NOT NULL,
  `phoneNumber` varchar(30) NOT NULL,
  `releasePermit` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`patientID`, `patientName`, `patientAge`, `bloodGroup`, `Gender`, `phoneNumber`, `releasePermit`) VALUES
('p0001', 'New Patient', '20', 'O(+)', 'Female', '1700000005', 'no'),
('p0002', 'Sobhan Mila', '21', 'A(-)', 'Female', '01700000000', 'no'),
('p0003', 'Foysal Rahman Nitu', '23', 'B(-)', 'Male', '01700000000', 'yes'),
('p0005', 'Patient Five', '22', 'A(+)', 'Male', '01800000000', 'yes'),
('p0006', 'Patient Six', '20', 'Unknown', 'Female', '01800000000', 'no'),
('p0009', 'Patient Nine', '25', 'A(+)', 'Male', '01800000000', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `treatment`
--

CREATE TABLE `treatment` (
  `patientID` varchar(30) NOT NULL,
  `Instruction` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `treatment`
--

INSERT INTO `treatment` (`patientID`, `Instruction`) VALUES
('p0001', 'Sample Treatment'),
('p0002', 'No Instruction Yet'),
('p0003', 'No Instruction Yet'),
('p0005', 'kjbjbvjbgNo Instruction Yetjhvhg\n'),
('p0006', 'No Instruction Yet'),
('p0009', 'No Instruction Yet\n\nsample');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`userName`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`UserName`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`patientID`);

--
-- Indexes for table `treatment`
--
ALTER TABLE `treatment`
  ADD PRIMARY KEY (`patientID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
